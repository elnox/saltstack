# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Kubernetes controller pillar

{%- from "controller/kubernetes.yaml" import data with context %}

kubernetes:
  controller:
    enable:           true
    service_enable:   true
    listen:           {{ data.ctrl.listen }}
    secure_port:      {{ data.ctrl.secure_port }}
    endpoint:         {{ data.ctrl.endpoint }}
    pubkey:           controller.crt
    privkey:          controller.key
    cluster_cidr:     {{ data.cidr.pods }}
    cidr_mask_size:   {{ data.cidr.mask }}
    domain:           {{ data.cluster.node }}-controller
