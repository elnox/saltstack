# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Kubernetes k8scerts pillar

{%- from "controller/kubernetes.yaml" import data with context %}

kubernetes:
  k8scerts:
    enable: true
    trust: "kali.drakonix.loc"
    certificates:
      {% if grains.get("group", "guest") == 'master' %}
      etcd:
        cn: {{ grains["id"] }}
        validity: 30
        remain: 10
        san:
          - "DNS:{{ data.api.listen.split('.')[2] }}.master"
          - "DNS:m{{ data.api.listen.split('.')[2] }}"
          - "IP Address:127.0.0.1"
          - "IP Address:{{ data.api.listen }}"
      apiserver:
        cn: {{ grains["id"] }}
        validity: 30
        remain: 10
        san:
          - "DNS:{{ data.api.listen.split('.')[2] }}.master"
          - "DNS:m{{ data.api.listen.split('.')[2] }}"
          - "IP Address:127.0.0.1"
          - "IP Address:{{ data.api.listen }}"
          - "IP Address:192.168.3.18"
      scheduler:
        cn: {{ grains["id"] }}
        validity: 30
        remain: 10
        san:
          - "DNS:{{ data.api.listen.split('.')[2] }}.master"
          - "DNS:m{{ data.api.listen.split('.')[2] }}"
          - "IP Address:127.0.0.1"
          - "IP Address:{{ data.api.listen }}"
      controller:
        cn: {{ grains["id"] }}
        validity: 30
        remain: 10
        san:
          - "DNS:{{ data.api.listen.split('.')[2] }}.master"
          - "DNS:m{{ data.api.listen.split('.')[2] }}"
          - "IP Address:127.0.0.1"
          - "IP Address:{{ data.api.listen }}"
      {% endif %}
      multus:
        cn: {{ grains["id"] }}
        validity: 30
        remain: 10
        san:
          - "DNS:{{ data.api.listen.split('.')[2] }}.multus"
          - "DNS:m{{ data.api.listen.split('.')[2] }}"
          - "IP Address:127.0.0.1"
          - "IP Address:{{ data.api.listen }}"
      {% if grains.get("group", "guest") == 'node' %}
      proxy:
        cn: {{ grains["id"] }}
        validity: 30
        remain: 10
        san:
          - "DNS:{{ data.proxy.listen.split('.')[2] }}.proxy"
          - "DNS:p{{ data.proxy.listen.split('.')[2] }}"
          - "IP Address:127.0.0.1"
      kubelet:
        cn: {{ grains["id"] }}
        validity: 30
        remain: 10
        san:
          - "DNS:{{ data.proxy.listen.split('.')[2] }}.node"
          - "DNS:n{{ data.proxy.listen.split('.')[2] }}"
          - "IP Address:127.0.0.1"
          - "IP Address:{{ data.proxy.listen }}"
      {% endif %}
      {% if grains.get("group", "guest") == 'gateway' %}
      dashboard:
        cn: {{ grains["id"] }}
        validity: 30
        remain: 10
        san:
          - "DNS:{{ data.proxy.listen.split('.')[2] }}.dashboard"
          - "DNS:d{{ data.proxy.listen.split('.')[2] }}"
          - "IP Address:127.0.0.1"
      {% endif %}
