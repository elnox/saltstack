# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Kubernetes scheduler pillar

{%- from "controller/kubernetes.yaml" import data with context %}

kubernetes:
  scheduler:
    enable:           true
    service_enable:   true
    endpoint:         {{ data.sched.endpoint }}
    pubkey:           scheduler.crt
    privkey:          scheduler.key
    domain:           {{ data.cluster.node }}-scheduler
