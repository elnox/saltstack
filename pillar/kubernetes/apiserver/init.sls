# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Kubernetes apiserver pillar

{%- from "controller/kubernetes.yaml" import data with context %}

kubernetes:
  apiserver:
    enable:           true
    service_enable:   true
    listen:           {{ data.api.listen }}
    secure_port:      {{ data.api.secure_port }}
    pubkey:           apiserver.crt
    privkey:          apiserver.key
    count:            {{ data.api.count }}
    cluster_ip_range: {{ data.cidr.services }}
    etcd_cluster:     {{ data.etcd }}
