# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Kubernetes tooling pillar

{% from "controller/kubernetes.yaml" import data with context %}

kubernetes:
  tooling:
    enable: true
    cluster_name: {{ data.cluster.name }}
