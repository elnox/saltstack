# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Kubernetes proxy pillar

{% from "controller/kubernetes.yaml" import data with context %}

kubernetes:
  proxy:
    enable:           true
    service_enable:   true
    listen:           {{ data.proxy.listen }}
    endpoint:         {{ data.proxy.endpoint }}
    cluster_cidr:     {{ data.cidr.pods }}
    pubkey:           proxy.crt
    privkey:          proxy.key
    domain:           {{ data.cluster.node }}-proxy
