# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Kubernetes multus pillar

{%- from "controller/kubernetes.yaml" import data with context %}

kubernetes:
  multus:
    enable:           true
    endpoint:         {{ data.multus.endpoint }}
    network:          {{ data.cidr.network }}
    cidr:             {{ data.multus.subnet }}/{{ data.cidr.mask }}
    bridge:           {{ data.multus.bridge }}
    pubkey:           multus.crt
    privkey:          multus.key
    domain:           {{ data.cluster.domain }}-kubelet
