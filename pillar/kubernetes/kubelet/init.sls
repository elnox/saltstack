# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Kubernetes kubelet pillar

{% from "controller/kubernetes.yaml" import data with context %}

kubernetes:
  kubelet:
    enable:           true
    service_enable:   true
    listen:           {{ data.kblt.listen }}
    endpoint:         {{ data.kblt.endpoint }}
    hostname:         {{ grains['id'] }}
    pubkey:           kubelet.crt
    privkey:          kubelet.key
    domain:           {{ data.cluster.domain }}-kubelet
