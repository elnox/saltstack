# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Kubernetes dashboard pillar

{% from "controller/kubernetes.yaml" import data with context %}

kubernetes:
  dashboard:
    enable:           true
    service_enable:   true
    endpoint:         {{ data.dash.endpoint }}
    pubkey:           dashboard.crt
    privkey:          dashboard.key
    domain:           {{ data.cluster.node }}-dashboard
