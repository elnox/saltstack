# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Kubernetes pillar

{%- from "root.yaml" import root with context %}

# compute a list of pillars we want to include
{%- set enabled = [] %}
{%- set components = [ "kube", "k8scerts", "multus", "apiserver", "scheduler", "controller", "kubelet", "proxy", "dashboard", "tooling" ] %}
{% for comp in components %}
{%-   do enabled.append(slspath + "." + comp) if root.get(comp) is sameas true %}
{% endfor %}

# include wanted pillars
include: {{ enabled }}
