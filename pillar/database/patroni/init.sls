# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Patroni pillar

{% from "controller/patroni.yaml" import data with context %}

patroni:
  enable: true
