# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Keydb pillar

{% from "controller/keydb.yaml" import data with context %}

keydb:
  enable:         true
  listen:         {{ data.listen }}
  port:           {{ data.port }}
  multi_master:   'yes'

  {%- if data.replicas | length %}
  active_replica: 'yes'
  replicaof:      {{ data.replicas | difference([data.listen]) }}
  replica_ro:     'no'
  {% endif %}

  secret:         "Aizai2kaiyohneeLioshahy8ieM8Ahgh"
