# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Zookeeper pillar

zookeeper:
  enable:  true
  cluster:
  - 172.16.1.19
  - 172.16.2.19
  - 172.16.3.19
