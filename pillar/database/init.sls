# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Database stuff pillar

{%- from "root.yaml" import root with context %}

# compute a list of pillars we want to include
{% set enabled = [] %}
{% set components = [ "keydb", "patroni", "zookeeper" ] %}
{% for comp in components %}
{%-   do enabled.append(slspath + "." + comp) if root.get(comp) is sameas true %}
{% endfor %}

# include wanted pillars
include: {{ enabled }}

# helper, check which components have been enabled
database: {{ enabled }}
