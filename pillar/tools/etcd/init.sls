# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Etcd pillar

{% from "controller/etcd.yaml" import data with context %}

etcd:
  enable:         true
  alias:          {{ data.nodename }}
  listen:         {{ data.listen }}
  log_level:      info
  state:          {{ grains.get('etcd', 'new') }}
  database_size:  2147483648
  cluster:        {{ data.servers }}
  certificates:
    etcd:
      ca_server:  'kali.drakonix.loc'
      cn:         {{ grains["id"] }}
      validity:   30
      remain:     10
      san:
      - "DNS:{{ data.listen.split('.')[2] }}.etcd"
      - "DNS:e{{ data.listen.split('.')[2] }}"
      - "IP Address:{{ data.listen }}"
      - "IP Address:127.0.0.1"
    peers:
      ca_server:  'kali.drakonix.loc'
      cn:         {{ grains["id"] }}
      validity:   30
      remain:     10
      san:
      - "DNS:{{ data.listen.split('.')[2] }}.etcd"
      - "DNS:e{{ data.listen.split('.')[2] }}"
      - "IP Address:{{ data.listen }}"
      - "IP Address:127.0.0.1"
