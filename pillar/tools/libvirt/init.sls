# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Libvirt pillar

{%- from "common.yaml" import common with context %}

libvirt:
  enable: true
  libvirtd:
    listen_tls: 1
    listen_tcp: 0
    tls_port: '16514'
    tcp_port: '16509'
    listen_addr: 0.0.0.0
    unix_sock_group: 'root'
    unix_sock_ro_perms: '0777'
    unix_sock_rw_perms: '0770'
    auth_unix_ro: 'none'
    auth_unix_rw: 'none'
    auth_tcp: 'none'
  {%- if common.group == "server" %}
  network:
    bridge-0:
      run: true
      forward: bridge
      bridge: br0
      autostart: true
    bridge-1:
      run: true
      forward: bridge
      bridge: br1
      autostart: true
  {%- endif %}
  pool:
    livecd:
      state: running
      type: dir
      target: /srv/livecd
      autostart: true
    {%- if common.group == "laptop" %}
    vg0:
      state: {{ "defined" if grains.get("bootstrap", false) else "running" }}
      type: logical
      target: /dev/vg0
      autostart: true
    {%- else %}
    vg1:
      state: {{ "defined" if grains.get("bootstrap", false) else "running" }}
      type: logical
      target: /dev/vg1
      autostart: true
    {%- endif %}
  volume:
    bullseye:
      pool: {{ "vg0" if common.group == "laptop" else "vg1" }}
      size: 15360
      format: raw
  {%- if common.group == "server" %}
  guest:
    etcd:
      image: bullseye
    master:
      image: bullseye
    zoo:
      image: bullseye
    kafka:
      image: bullseye
    {%- if common.host in ["blackbox", "whitebox"] %}
    node:
      image: bullseye
    {%- endif %}
    {%- if common.host in ["greenbox"] %}
    gateway:
      image: bullseye
    {%- endif %}
  {%- endif %}
