# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Bind pillar

{%- from "controller/bind.yaml" import data with context %}

bind:
  enable: true
  config:
    protocol:             4
    default_zones:        true
    enable_logging:       true
    options:
      zone-statistics:    'yes'
      listen-on-v6:       'port {{ data.port }} { ::1; }'
      allow-query:        '{ trusted; }'
      allow-query-cache:  '{ trusted; }'
      allow-transfer:     '{ trusted; }'
      allow-update:       '{ none; }'
      allow-recursion:    '{ trusted; }'
      dnssec-validation:  'auto'
      forwarders:         {{ data.forwarders }}
      version:            '{{ data.version }}'
      also-notify:        {{ data.notify }}
  statistics:
    local:
      bind:
        port:             8053
      allow:              [ 127.0.0.1 ]
  controls:
    local:
      allow:              [ "127.0.0.1/32", "::1/128" ]
      keys:               [ "rndc-key" ]
  configured_masters:
    dns_masters:          {{ data.masters }}
  keys:
    external:
      algorithm:          {{ data.algorithm }}
      secret:             {{ data.secret }}
  configured_acls:
    xfer:                 [ none ]
    trusted:              {{ data.networks }}
  configured_views:
    internal:
      root_zones:         true
      options:
        recursion:        'yes'
        match_clients:    {{ data.views.internal }}
        allow_notify:     '{ none; }'
      configured_zones:   {{ salt['infra.get_dnsview'](data.infra, data.sld) }}
  available_zones:        {{ salt['infra.get_dnszone'](data.infra, data.soa) }}
  logging:
    channel:
      syslog:
        syslog:     named
        severity:   warning
      {%- for file in data.logger %}
      {{ file }}_log:
        file:       {{ file }}
        size:       '5m'
        versions:   '3'
        severity:   dynamic
      {%- endfor %}
    category:
      {%- for file in data.logger %}
      {{ file }}: [ {{ file }}_log ]
      {%- endfor %}
