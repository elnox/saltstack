# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Gns3 pillar

{%- from "common.yaml" import common with context %}

gns3:
  enable:         true
  server_enable:  true
  client_enable:  {{ common.group == "laptop" }}
  remote:         {{ common.group == "server" }}
