# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Docker pillar

docker:
  enable:           true
  service_enable:   true
  bridge:           "docker0"
  debug:            true
  root_data:        {{ "/srv/docker" if grains['group'] in [ "server", "laptop" ] else "/var/lib/docker" }}
  registry:
    enable:         {{ grains['host'] == "greenbox" }}
    port:           5050
    insecure:       "registry.drakonix.loc"
  log:
    labels:
    - io.kubernetes.pod.name
    - io.kubernetes.pod.namespace
    - io.kubernetes.pod.uid
    - tier
    - component
    tag: {{ 'k8s.docker-container.{{.ImageName}}.{{.Name}}.{{.ID}}' }}
