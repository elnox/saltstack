# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Consul pillar

{% from "controller/consul.yaml" import data with context %}

consul:
  enable: true

  config:
    agent:
      - 'user="consul"'
      - 'group="consul"'
      - 'command_args="-config-dir=/etc/consul.d/agent"'
      - 'GOMAXPROCS=2'

    server:
      - 'command_args="-config-dir=/etc/consul.d"'
      - 'GOMAXPROCS=2'

  {%- if grains['id'] in data.servers %}
  server:
    datacenter:             {{ data.dc }}
    data_dir:               /var/lib/consul
    node_name:              {{ data.nodename }}
    advertise_addr:         {{ data.eth0 }}
    bind_addr:              {{ data.eth0 }}
    client_addr:            0.0.0.0
    ports:
      dns:                  8600
      http:                 8500
      serf_lan:             8301
      serf_wan:             8302
      server:               8300
    retry_join:             {{ data.servers }}
    ui:                     true
    server:                 true
    bootstrap_expect:       {{ data.servers | length }}
    encrypt:                {{ data.secret }}
    enable_syslog:          true
    enable_debug:           false
    log_level:              warn
  {%- endif %}

  {%- if grains['id'] in data.servers %}
  agent:
    datacenter:             {{ data.dc }}
    data_dir:               /var/lib/consul/data
    node_name:              {{ data.nodename }}-agent
    bind_addr:              {{ data.eth1 }}
    retry_join:             {{ data.servers }}
    ui:                     false
    server:                 false
    ports:
      dns:                  9600
      http:                 9500
    encrypt:                {{ data.secret }}
    enable_syslog:          true
    enable_debug:           false
    log_level:              warn
  {%- endif %}
