# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Kafka pillar

kafka:
  enable:  true
  cluster:
  - 192.168.1.20
  - 192.168.2.20
  - 192.168.3.20
  zookeeper:
  - "172.16.1.19:2181"
  - "172.16.2.19:2181"
  - "172.16.3.19:2181"
  config:
    partitions: 3
    factor: 2
    retention: 24
