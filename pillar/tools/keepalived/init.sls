# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Keepalived pillar

{% from "controller/keepalived.yaml" import data with context %}

keepalived:
  enable: true
  config:
    # Global
    -
      global_defs:
        smtp_connect_timeout: 30
        router_id: {{ data.router_id }}

    # VRRP SYNC
    -
      vrrp_sync_group:
        VG1:
          group: [ {{ data.vrrp_group }} ]

    # VRRP SCRIPT
    #-
    #  vrrp_script:
    #    chk_haproxy:
    #      script: '"/usr/bin/killall -0 haproxy"'
    #      interval: 9
    #      timeout: 3
    #      weight: 20
    #      rise: 2
    #      fall: 4

    # VRRP INSTANCE
    -
      vrrp_instance:
        {{ data.vrrp_group }}:
          {%- set peers = [] %}
          {%- for server, spec in data.get('cluster', {}) | dictsort %}
          {%-   if grains['host'] == server %}
          state:              {{ "MASTER" if spec.master else "BACKUP" }}
          virtual_router_id:  51
          priority:           {{ spec.get("priority", "50") }}
          mcast_src_ip:       {{ spec.ip }}
          unicast_src_ip:     {{ spec.ip }}
          interface:          {{ spec.iface }}
          advert_int:         1
          {%-   else %}
          {%-     do peers.append(spec.ip) %}
          {%-   endif %}
          {%- endfor %}
          unicast_peer:       {{ peers }}
          authentication:
            auth_type:        PASS
            auth_pass:        1111
          virtual_ipaddress:  {{ data.vip }}

    #      track_script:
    #        - chk_haproxy

    # VIRTUAL SERVER
    -
      virtual_server: {}
