# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Haproxy pillar

haproxy:
  enable:  true

  scripts:
    200.lua: /etc/haproxy/modules.d

  global:
    lua-load: /etc/haproxy/modules.d/200.lua
    ssl-default-bind-ciphersuites: "TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256"
    ssl-default-bind-options: "prefer-client-ciphers no-sslv3 no-tlsv10 no-tlsv11 no-tlsv12 no-tls-tickets"
    ssl-default-server-ciphersuites: "TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256"
    ssl-default-server-options: "no-sslv3 no-tlsv10 no-tlsv11 no-tlsv12 no-tls-tickets"

  {% if grains.get("group", "guest") == 'gateway' %}
  frontends:
    ft_apiserver:
      bind: "*:443 transparent"
      default_backend: "bk_apiserver"

  backends:
    bk_apiserver:
      option: [ 'tcp-check' ]
      servers:
        {% for ip in [ "172.16.1.17", "172.16.2.17", "172.16.3.17" ] %}
        {{ loop.index }}.master:
          host: {{ ip }}
          port: 6443
          check: 'check port 6443'
          extra: 'inter 60s'
        {% endfor %}
  {% endif %}

  {% if grains.get("group", "guest") == 'node' %}
  listens:
    apiserver:
      bind:
        - "127.0.0.1:6443"
      mode: tcp
      option: [ 'tcplog', 'tcp-check' ]
      servers:
        {% for ip in [ "172.16.1.17", "172.16.2.17", "172.16.3.17" ] %}
        {{ loop.index }}.master:
          host: {{ ip }}
          port: 6443
          check: 'check port 6443'
          extra: 'inter 60s'
        {% endfor %}
  {% endif %}

  {% if grains.get("group") == 'server' %}
  certificates:
    registry:
      cn: "registry.drakonix.loc"
      validity: 30
      remain: 10
      san:
        - "DNS:registry.drakonix.loc"
        - "IP Address:127.0.0.1"
        - "IP Address:192.168.0.100"
    dashboard:
      cn: "dashboard.drakonix.loc"
      validity: 30
      remain: 10
      san:
        - "DNS:dashboard.drakonix.loc"
        - "IP Address:127.0.0.1"
        - "IP Address:192.168.0.100"
    kafka:
      cn: "kafka.drakonix.loc"
      validity: 30
      remain: 10
      san:
        - "DNS:kafka.drakonix.loc"
        - "IP Address:127.0.0.1"
        - "IP Address:192.168.0.100"

  frontends:
    ft_catchall:
      bind: "127.0.0.1:8888"
      mode: http
      http-request:
        - "use-service lua.ok"

    ft_http_router:
      bind: "192.168.0.100:80"
      mode: http
      default_backend: "bk_http_router"

    ft_https_router:
      bind: "192.168.0.100:443 ssl crt /etc/haproxy/ssl.d/"
      mode: tcp
      acl:
        - "dashboard ssl_fc_sni_end dashboard.drakonix.loc"
        - "registry ssl_fc_sni_end registry.drakonix.loc"
      use_backends:
        - "bk_dashboard if dashboard"
        - "bk_registry if registry"
      default_backend: "bk_https_router"

    ft_kafka_router:
      bind: "192.168.0.100:9092"
      mode: tcp
      default_backend: "bk_kafka"

  backends:
    bk_http_router:
      mode: http
      servers:
        lua:
          host: 127.0.0.1
          port: 8888

    bk_https_router:
      mode: tcp
      servers:
        lua:
          host: 127.0.0.1
          port: 8888

    bk_registry:
      option: [ 'tcp-check' ]
      mode: tcp
      servers:
        registry:
          host: "172.16.3.1"
          port: 5050
          check: 'check port 5050'
          extra: 'inter 60s ssl verify none'

    bk_dashboard:
      option: [ 'tcp-check' ]
      mode: tcp
      servers:
        dashboard:
          host: "192.168.3.18"
          port: 8443
          check: 'check port 8443'
          extra: 'inter 60s ssl verify none'

    bk_kafka:
      option: [ 'tcp-check' ]
      mode: tcp
      servers:
        k1:
          host: "192.168.1.20"
          port: 5050
          check: 'check port 9092'
          extra: 'inter 60s'
        k2:
          host: "192.168.2.20"
          port: 5050
          check: 'check port 9092'
          extra: 'inter 60s'
        k3:
          host: "192.168.3.20"
          port: 5050
          check: 'check port 9092'
          extra: 'inter 60s'

  {% endif %}
