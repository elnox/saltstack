# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Pillar top file
#
# default salt environment
{{saltenv}}:

  # applied everywhere
  '*':
    - common
    - storage
    - tools
    - database
    - kubernetes
    - saltstack
    - test
