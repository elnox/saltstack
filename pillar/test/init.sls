# -*- coding: utf-8 -*-
# vim: ft=sls
#
# test pillar

{%- from "common.yaml" import common with context %}

test:
  master:   {{ salt['infra.get_dnsips'](common.infra, "master") }}
  slave:    {{ salt['infra.get_dnsips'](common.infra, "slave") }}
  all:      {{ salt['infra.get_dnsips'](common.infra) }}
  network:  {{ salt['infra.get_network'](common.infra, common.sld) }}
  dataset:  {}
