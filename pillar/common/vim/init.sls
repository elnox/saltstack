# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Vim pillar

vim:

  skip_defaults: true
  allow_localrc: false
  allow_vimplug: false

  config:
    colors: desert
    syntax: 'on'
    filetype: 'plugin indent on'

    autocmd:
      FileType:
        sls: 'setlocal tabstop=2 softtabstop=2 shiftwidth=2'
        php,html,tpl: "set softtabstop=4"
        make: 'setlocal noexpandtab'
      BufRead,BufNewFile:
        /etc/salt/*/*.conf: 'set filetype=sls'

  settings:
    nocompatible: ~
    ruler: ~
    showmode: ~
    showcmd: ~
    showmatch: ~
    ttyfast: ~
    ignorecase: ~
    smartcase: ~
    incsearch: ~
    hlsearch: ~
    autowrite: ~
    autoread: ~
    hidden: ~
    nobackup: ~
    history: 1000
    backspace: indent,eol,start
    laststatus: 2
    wildmenu: ~
    wildmode: longest,list,full
    noswapfile: ~
    directory: /tmp
    updatetime: 100
    ai: ~
    tabstop: 4
    shiftwidth: 4
    softtabstop: 4
    expandtab: ~
    encoding: utf-8
    fileencoding: utf-8
    termencoding: utf-8
    list: ~
    listchars: 'tab:›\ ,trail:•,extends:#,nbsp:.'
    textwidth: 0
