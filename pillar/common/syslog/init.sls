# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Syslog-ng pillar

{%- from "common.yaml" import common with context %}

syslog:

  include:
    - scl.conf
  options:
    - threaded:         yes
    - chain_hostnames:  no
    - stats_freq:       43200
    - mark_freq:        3600
    - owner:            root
    - group:            adm
    - perm:             0644
    - dir_perm:         0740
    - create_dirs:      yes
    - use_fqdn:         no
    - log_fifo_size:    4096
    - keep_hostname:    yes
    - use_dns:          yes
    - dns_cache:        yes

  source:
    - src:
      - internal: null
      - system:   null
    - loc:
      - udp:
        - ip:   127.0.0.1
        - port: 514

  destination:
      - d_messages:
        - file: /var/log/messages
      - d_syslog:
        - file: /var/log/syslog/$FACILITY.log
      - d_console:
        - file: /dev/tty12

  filter:
    - f_messages:
      - level: =info..emerg
    - f_exclude:
      - not message: ".*192.168.*.1.*"

  template:
    - remote:
      - template: "$DATE $HOST $FACILITY $LEVEL $MSGHDR$MSG\\n"
      - template_escape: no

  log:
    -
      - source:       =src
      - filter:       =f_exclude
      - destination:  =d_messages
    -
      - source:       =src
      - filter:       =f_exclude
      - destination:  =d_syslog
    -
      - source:       =src
      - destination:  =d_console
