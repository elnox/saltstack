# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Apt pillar

{%- from "controller/apt.yaml" import data with context %}

apt:
  default_url:          {{ data.mirror }}
  remove_preferences:   {{ data.clean }}
  remove_sources_list:  {{ data.clean }}
  sources_list_clean:   {{ data.clean }}
  preferences_clean:    {{ data.clean }}

  conf_d:
    01norecommend:
      'APT::Install-Recommends': "0"
      'APT::Install-Suggests': "0"
    30release:
      'APT::Default-Release': {{ data.distro }}
    40dpkg-options:
      'Dpkg::Options':
        - '--force-confdef'
        - '--force-confold'
    99-force-ipv4:
        'Acquire::ForceIPv4': 'true'

  listchanges:
    profiles:
      apt:
        frontend:       pager
        email_address:  root
        save_seen:      /var/lib/apt/listchanges.db
        which:          news
        email_format:   text
        confirm:        'false'
        headers:        'false'
        reverse:        'false'

  preferences:
    stable.pref:
      pin:              "release a=stable"
      priority:         900

    {%- if data.host in data.testing %}
    testing.pref:
      pin:      "release a=testing"
      priority: 400
    {%- endif %}

    {%- if data.host in data.unstable %}
    unstable.pref:
      pin:      "release a=unstable"
      priority: 50
    {%- endif %}

  # repositories setup
  repositories:

    # kali standard
    {%- if grains["os"] == "Kali" %}
    kali-{{ data.distro }}:
      distro:     {{ data.distro }}
      url:        {{ data.mirror.get(grains["os"] | lower) }}
      comps:      [ main, contrib, non-free ]
      type:       [ binary ]
    {%- endif %}

    # debian standard
    {%- if grains["os"] == "Debian" %}
    debian-{{ data.distro }}:
      distro:     {{ data.distro }}
      url:        {{ data.mirror.get(grains["os"] | lower) }}
      {%- if grains.get("group", "")  in data.nonfree %}
      comps:      [ main, contrib, non-free ]
      {%- else %}
      comps:      [ main, contrib ]
      {%- endif %}
      type:       [ binary, source ]

    security-{{ data.distro }}:
      distro:     {{ data.distro ~ "-security" }}
      url:        "http://security.debian.org/debian-security"
      {%- if grains.get("group", "")  in data.nonfree %}
      comps:      [ main, contrib, non-free ]
      {%- else %}
      comps:      [ main, contrib ]
      {%- endif %}
      type:       [ binary, source ]

    updates-{{ data.distro }}:
      distro:     {{ data.distro ~ "-updates" }}
      url:        {{ data.mirror.get(grains["os"] | lower) }}
      {%- if grains.get("group", "")  in data.nonfree %}
      comps:      [ main, contrib, non-free ]
      {%- else %}
      comps:      [ main, contrib ]
      {%- endif %}

    backports-{{ data.distro }}:
      distro:     {{ data.distro ~ "-backports" }}
      url:        {{ data.mirror.get(grains["os"] | lower) }}
      comps:      [ main, contrib ]

    {%-   if grains.get('group', '')  in data.testing %}
    testing-{{ data.distro }}:
      distro:     "testing"
      url:        {{ data.mirror.get(grains["os"] | lower) }}
      comps:      [ main, contrib ]
    {%-   endif %}

    {%-   if grains.get('group', '')  in data.unstable %}
    unstable-{{ data.distro }}:
      distro:     "unstable"
      url:        {{ data.mirror.get(grains["os"] | lower) }}
      comps:      [ main, contrib ]
    {%-   endif %}
    {%- endif %}

    # custom repos
    {%- for repos, dataset in data.repos | dictsort %}
    {%-   if dataset.groups is defined and data.group in dataset.groups %}
    {{ repos }}-{{ data.distro }}:
      distro:     {{ dataset.config.distro | default("stable") }}
      comps:      [ {{ dataset.config.comps | default("main") }} ]
      type:       [ binary ]
      url:        {{ dataset.config.url }}
      {%- if dataset.config.key_url is defined %}
      key_url:    {{ dataset.config.key_url }}
      {%- elif dataset.config.keyid is defined %}
      keyid:      {{ dataset.config.keyid }}
      keyserver:  {{ dataset.config.keyserver }}
      {%- endif %}
    {%-   elif dataset.groups is not defined %}
    {{ repos }}-{{ data.distro }}:
      distro:     {{ dataset.config.distro | default("stable") }}
      comps:      [ {{ dataset.config.comps | default("main") }} ]
      type:       [ binary ]
      url:        {{ dataset.config.url }}
      {%- if dataset.config.key_url is defined %}
      key_url:    {{ dataset.config.key_url }}
      {%- elif dataset.config.keyid is defined %}
      keyid:      {{ dataset.config.keyid }}
      keyserver:  {{ dataset.config.keyserver }}
      {%- endif %}
    {%-   endif %}
    {%- endfor %}
