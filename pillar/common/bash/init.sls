# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Bash pillar

{%- from "common.yaml" import common with context %}

bash:
  host:           {{ common.host }}
  cell:           {{ common.domain }}
  users:          {{ [ "root" ] + common.users }}
