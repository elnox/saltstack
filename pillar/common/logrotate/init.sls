# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Logrotate pillar

{%- from "common.yaml" import common with context %}

logrotate:

  default_config:
    weekly:     True
    rotate:     4
    dateext:    True
    compress:   True
    notifempty: True
    nomail:     True
    noolddir:   True

  login_records_jobs: {{ not common.distro in [ 'buster', 'bullseye' ] }}

  jobs:
    syslog-ng:
      present: true
      messages:
        path:
          - /var/log/messages
          - /var/log/remote.log
        config:
          - rotate 0
          - daily
          - missingok
          - create 0644 root adm
          - sharedscripts
          - postrotate
          -   /etc/init.d/syslog-ng reload > /dev/null 2>&1 || true
          - endscript
      others:
        path:
          - /var/log/syslog/*.log
        config:
          - rotate 7
          - daily
          - missingok
          - create 0644 root adm
          - sharedscripts
          - postrotate
          -   /etc/init.d/syslog-ng reload > /dev/null 2>&1 || true
          - endscript
