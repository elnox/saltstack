# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Network pillar

{%- from "common.yaml" import common with context %}

network:
  enable: true
  watch:  {{ common.bootstrap }}
  interfaces:
    {%- for iface, data in salt['infra.get_network'](common.infra, common.sld) | dictsort %}
    {%-   if data.manual | default(false) %}
    {{ iface }}:
      proto:            "manual"
    {%-   elif data.bonding is defined %}
    {{ iface }}:
      proto:            "manual"
      options:
        bond-mode:      "balance-alb"
        bond-miimon:    100
        bond-slaves:    "none"
    {%-     for interface in data.bonding %}
    {{ interface }}:
      proto:            "manual"
      options:
        bond-master:    {{ iface }}
    {%-     endfor %}
    {%-   elif data.bridge is defined %}
    {{ iface }}:
      auto:             true
      proto:            "static"
      address:          {{ data.ip }}
      gateway:          {{ data.get("gateway", "") }}
      nameservers:      {{ data.get("resolv", []) }}
      options:
        dns-search:     {{ data.get("dns-search", "") }}
        bridge-ports:   {{ data.bridge }}
        bridge-fd:      0
        bridge-hello:   2
        bridge-maxage:  12
        bridge-stp:     'off'
        hwaddress:      {{ data.mac }}
    {%-   else %}
    {{ iface }}:
      auto:             {{ data.get("auto", false) }}
      proto:            "static"
      address:          {{ data.ip }}
      gateway:          {{ data.get("gateway", "") }}
      nameservers:      {{ data.get("resolv", []) }}
      options:
        dns-search:     {{ data.get("dns-search", "") }}
    {%-   endif %}
    {%- endfor %}
