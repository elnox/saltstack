# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Resolver pillar

{%- from "common.yaml" import common with context %}

resolver:
  enable:       true
  domain:       {{ common.sld }}
  nameservers:  {{ salt['infra.get_resolver'](common.infra, common.sld) }}
  searchpaths:  [ {{ common.sld }} ]
  options:      [ 'rotate', 'timeout:1', 'attempts:5' ]
