# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Sysctl pillar

{%- from "common.yaml" import common with context %}

sysctl:
  settings:
    net.core.rmem_max: 13421772             # default: 212992
    net.core.wmem_max: 13421772             # default: 212992
    net.ipv4.tcp_rmem: 4096 131072 13421772 # default: 4096 87380 6291456
    net.ipv4.tcp_wmem: 4096 65536 13421772  # default: 4096 16384 4194304
    net.core.netdev_max_backlog: 5000       # default: 1000
    net.ipv4.tcp_fin_timeout: 30            # default: 60
    net.ipv4.tcp_keepalive_intvl: 30        # default: 75
    net.ipv4.tcp_keepalive_probes: 5        # default: 9
    net.ipv4.tcp_slow_start_after_idle: 0   # default: 1
    net.ipv4.tcp_mtu_probing: 1             # default: 0
    net.core.somaxconn: 4096                # default: 128
    {%- if common.group == 'server' %}
    net.ipv4.ip_nonlocal_bind: 1            # default: 0
    {%- endif %}
