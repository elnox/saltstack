# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Common stuff pillar

{%- from "root.yaml" import root with context %}

# wanted everywhere
{% set mandatories  = [ 'apt', 'bash', 'cron', 'debian', 'hostname', 'locale', 'logrotate', 'ntp', 'openssh', 'repos', 'sysctl', 'syslog', 'timezone', 'vim' ] %}

# explicitly enabled on a per group basis
{% set optionals    = [ 'network', 'resolver' ] %}

# compute a list of pillars we want to include
{%- set enabled = [] %}
{%- for component in mandatories %}
{%-   do enabled.append(slspath + "." + component) %}
{%- endfor %}
{%- for component in optionals %}
{%-   do enabled.append(slspath + "." + component) if root.get(component) is sameas true %}
{%- endfor %}

# include wanted pillars
include: {{ enabled }}

# helper, check which components have been enabled
common: {{ enabled }}
