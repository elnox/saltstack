# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Git repository pillar

{%- from "common.yaml" import common with context %}

repos:

  deploy:
    prompt:
      url:      https://github.com/magicmonty/bash-git-prompt.git
      target:   /etc/bash/prompt
      reset:    true

    {%- if common.host in common.saltstack.masters %}
    salt-master:
      url:      https://gitlab.com/elnox/saltstack.git
      target:   '/srv/saltstack'
      rev:      'master'
      branch:   'master'
      reset:    true
    {%- endif %}
