# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Ntp pillar

{%- from "common.yaml" import common with context %}

ntp:
  settings: ['nomodify', 'nopeer', 'noquery', 'limited', 'kod']
  pool:
    - 0.debian.pool.ntp.org
    - 1.debian.pool.ntp.org
    - 2.debian.pool.ntp.org
    - 3.debian.pool.ntp.org
