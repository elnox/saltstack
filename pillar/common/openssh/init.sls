# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# OpenSSH pillar

{%- from "common.yaml" import common with context %}

openssh:
  sshd:
    PermitRootLogin: 'prohibit-password'
    ListenAddress: '0.0.0.0'
    PasswordAuthentication: 'no'
    UsePAM: 'yes'
    AcceptEnv:
      - "LANG LC_*"
      - "COLORTERM"
      - "PERSIST"
      - "BASTION"
  auth:
    root:
      genkeys:
        id_ecdsa:
          enc: ecdsa
          bits: 521
        id_ed25519:
          enc: ed25519
    yoda:
      genkeys:
        id_ecdsa:
          enc: ecdsa
          bits: 521
        id_ed25519:
          enc: ed25519
      authorized:
        kali:
          enc: "ed25519"
          pubkey: "AAAAC3NzaC1lZDI1NTE5AAAAICWggXkwE1PF07vGlafEdbdVt+wTFYWZUu9fClA438Yn"
          comment: "yoda@kali.drakonix.net"
