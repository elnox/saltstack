# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Locale pillar

{%- from "common.yaml" import common with context %}

locale:
  present:
    - "en_US ISO-8859-1"
    - "en_US.UTF-8"
    - "fr_FR ISO-8859-1"
    - "fr_FR@euro ISO-8859-15"
    - "fr_FR.UTF-8"
  default:
    name:     "en_US.UTF-8"
    require:  "en_US.UTF-8"
