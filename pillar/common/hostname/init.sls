# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Hostname pillar

{%- from "common.yaml" import common with context %}

hostname:
  fqdn:           {{ common.fqdn }}
  name:           {{ common.host }}
