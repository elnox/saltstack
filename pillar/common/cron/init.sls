# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Cron pillar

{%- from "common.yaml" import common with context %}

cron:
  env:
    root:
      -
        present: true
        name: 'PATH'
        value: '/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin'

  tasks:
    {%- if common.host in common.saltstack.masters %}
    salt_repos:
      type: 'present'
      name: 'salt-call state.apply common.repos concurrent=1 > /dev/null 2>&1'
      user: 'root'
      minute: '*/1'
      comment: 'Every 5 minute ensure saltstack repositories is up 2 date: sync saltstack repos'
    {%- else %}
    {}
    {%- endif %}
