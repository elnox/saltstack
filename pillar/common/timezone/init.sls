# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Timezone pillar

{%- from "common.yaml" import common with context %}

timezone:
  name: 'Europe/Paris'
  utc:  False
