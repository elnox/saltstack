# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Saltstack master pillar

{%- from "controller/saltstack.yaml" import data with context %}

saltstack:
  master:
    enable:               true
    install:              true
    {% if data.upgrade is sameas true %}
    version:              {{ data.version }}
    upgrade:              true
    {% endif %}
    default_include:      'master.d/*.conf'
    interface:            '0.0.0.0'
    timeout:              120
    con_cache:            true
    extension_modules:    {{ data.rootfs }}/extmods
    worker_threads:       23
    zmq_backlog:          1000
    pub_hwm:              1000
    ipc_write_buffer:     'dynamic'
    batch_safe_size:      20
    batch_safe_limit:     20
    state_verbose:        false
    state_output:         'changes'
    state_output_diff:    true
    auto_accept:          true
    autosign_grains_dir:  /etc/salt/autosign.d
    fileserver_backend:
      - roots
    file_roots:
      base:
        - {{ data.rootfs }}/extmods
        - {{ data.rootfs }}/files
        - {{ data.rootfs }}/states
    pillar_roots:
      base:
        - {{ data.rootfs }}/root
        - {{ data.rootfs }}/pillar
    ext_pillar_first: true
    reactor_worker_threads: 16
    reactors:
      - 'salt/beacon/*/cert_info/*':
        - '{{ data.rootfs }}/reactor/certificate.sls'
      - 'certificate/*/update/*':
        - '{{ data.rootfs }}/reactor/beacon.sls'
    peer:
      ".*":
        - x509.sign_remote_certificate
