# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Saltstack minion pillar

{%- from "controller/saltstack.yaml" import data with context %}

saltstack:
  minion:
    enable:             true
    {% if data.upgrade is sameas true %}
    version:            {{ data.version }}
    upgrade:            true
    {% endif %}
    id:                 {{ data.minion_fqdn }}
    master:             {{ data.master_ip }}
    hash_type:          sha256
    state_verbose:      false
    state_output:       "changes"
    state_output_diff:  false
    startup_states:     {{ data.startup_states }}
    log_level:          error
    grains:
      fqdn:             {{ data.minion_fqdn }}
      {%- if data.grains is defined %}
      {%-   for k, v in data.grains.get('fixed', {}) | dictsort %}
      {{ k }}: {{ v }}
      {%-   endfor %}
      {%-   for item in data.grains.get('wanted', []) %}
      {%-     if grains[item] is defined %}
      {{ item }}: {{ grains[item] }}
      {%-     endif %}
      {%-   endfor %}
      {%- endif %}

    beacons:
      cert_info:
        files: {{ grains.get('certificates', []) }}
        notify_days: 10
        interval: 3600
