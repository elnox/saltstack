# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Saltcloud pillar

{%- from "controller/saltstack.yaml" import data with context %}

saltstack:

  cloud:
    enable:               true
    {% if data.upgrade is sameas true %}
    version:            {{ data.version }}
    {% endif %}
    bootstrap: bootstrap.sh
    providers:
      drakonix:
        greenbox:
          driver: libvirt
          url: "qemu+ssh://root@192.168.3.1/system?socket=/var/run/libvirt/libvirt-sock"
