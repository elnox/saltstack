# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Saltstack authority pillar

{%- from "controller/saltstack.yaml" import data with context %}

saltstack:
  authority:
    enable: true
    trust: "kali.drakonix.loc"
  certificates:
    saltstack:
      cn: {{ grains["id"] }}
      validity: 30
      remain: 10
      {%- if grains.get("group", "guest") == "laptop" %}
      san:
        - "DNS:{{ data.minion_fqdn }}"
        - "IP Address:127.0.0.1"
        - "IP Address:{{ data.master_ip }}"
      {%- endif %}
    test:
      absent: true
      cn: {{ grains["id"] }}
      validity: 1
      remain: 1
