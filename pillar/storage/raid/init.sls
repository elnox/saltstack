# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Raid pillar

raid:
  enable: true
  array:
    md2:
      level: 1
      devices: [ "/dev/sda5", "/dev/sdb5" ]
    md3:
      level: 1
      devices: [ "/dev/sda6", "/dev/sdb6" ]
