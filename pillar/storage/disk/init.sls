# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Disk pillar

disk:
  enable: true

  device:
    sda:
      name: /dev/sda
      type: msdos
      startsector: 0
      unit: "GB"
      partitions:
        -
          size: 15
          raid: true
          kind: primary
        -
          size: 10
          raid: true
          kind: primary
        -
          type: linux-swap
          size: 4
          kind: primary
        -
          size: {{ 1950 if grains["host"] == "greenbox" else 970 }}
          kind: extended
        -
          size: 100
          raid: true
          kind: logical
        -
          size: 200
          raid: true
          kind: logical
    sdb:
      name: /dev/sdb
      type: msdos
      startsector: 0
      unit: "GB"
      partitions:
        -
          size: 15
          raid: true
          kind: primary
        -
          size: 10
          raid: true
          kind: primary
        -
          type: linux-swap
          size: 4
          kind: primary
        -
          size: {{ 1950 if grains["host"] == "greenbox" else 970 }}
          kind: extended
        -
          size: 100
          raid: true
          kind: logical
        -
          size: 200
          raid: true
          kind: logical
