# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Storage stuff pillar

{%- from "root.yaml" import root with context %}

# compute a list of pillars we want to include
{%- set enabled = [] %}
{%- set components = [ "disk", "raid", "lvm" ] %}
{%- for comp in components %}
{%-   do enabled.append(slspath + "." + comp) if root.get(comp) is sameas true %}
{%- endfor %}

# include wanted pillars
include: {{ enabled }}

# helper, check which components have been enabled
storage: {{ enabled }}
