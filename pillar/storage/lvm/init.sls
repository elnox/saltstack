# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Lvm pillar

{%- from "common.yaml" import common with context %}

lvm:
  enable: true
  pv:
    create:
      /dev/md2: {}
      /dev/md3: {}
  vg:
    create:
      vg0:
        devices: [ "/dev/md2" ]
      vg1:
        devices: [ "/dev/md3" ]
  lv:
    create:
      srv:
        vgname: vg0
        size:   50G
        options:
          monitor: y
          force: true
        format: ext4
        mount:
          target: /srv
          opts: "rw"
          persist: true
