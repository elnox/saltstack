# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Lvm meta state

{%- from slspath ~ "/map.jinja" import lvm with context %}

{%- if lvm.enable is sameas true %}

lvm_pkgs:
  pkg.installed:
    - pkgs: {{ lvm.pkgs | json }}
    - failhard: True

{%- if lvm.pv is defined and 'create' in lvm.pv %}
{%-   for dev, opts in lvm.pv.create | dictsort() %}
lvm_pv_create_{{ dev }}:
  lvm.pv_present:
    - name: {{ dev }}
    {%- if opts and 'options' in opts %}
    {%- for opt, value in opts | dictsort() %}
    - {{ opt }}: {{ value }}
    {%- endfor %}
    {%- endif %}
{%-   endfor %}
{%- endif %}

{%- if lvm.vg is defined and 'create' in lvm.vg %}
{%-   for vg, vgdata in lvm.vg.create | dictsort %}
lvm_vg_create_{{ vg }}:
  lvm.vg_present:
    - name: {{ vg }}
    - devices: {{ vgdata['devices'] | json }}
    {%- if 'options' in vgdata %}
    {%- for opt, value in vgdata.options | dictsort() %}
    - {{ opt }}: {{ value }}
    {%- endfor %}
    {%- endif %}
{%-   endfor %}
{%- endif %}

{%- if lvm.lv is defined and 'create' in lvm.lv %}
{%-   for lv, lvdata in lvm.lv.create | dictsort %}
lvm_lv_create_{{ lv }}:
  lvm.lv_present:
    - name: {{ lv }}
    - vgname: {{ lvdata.vgname }}
    {%- if 'devices' in lvdata %}
    - devices: {{ lvdata.devices }}
    {%- endif %}
    {%- if 'size' in lvdata %}
    - size: {{ lvdata.size }}
    {%- endif %}
    {%- if 'options' in lvdata %}
    {%- for opt, value in lvdata.options | dictsort() %}
    - {{ opt }}: {{ value }}
    {%- endfor %}
    {%- endif %}
    - require:
      - lvm: lvm_vg_create_{{ lvdata.vgname }}

{%-     if 'format' in lvdata %}
lvm_lv_format_{{ lv }}:
  blockdev.formatted:
    - name: {{ "/dev/" ~ lvdata.vgname ~ "/" ~ lv }}
    - fs_type: {{ lvdata.format }}
    - require:
      - lvm: lvm_lv_create_{{ lv }}
  {%- if 'mount' in lvdata %}
  mount.mounted:
    - name: {{ lvdata.mount.target }}
    - device: {{ "/dev/" ~ lvdata.vgname ~ "/" ~ lv }}
    - fstype: {{ lvdata.format }}
    {%- if 'opts' in lvdata.mount %}
    - opts: {{ lvdata.mount.opts }}
    {%- endif %}
    - persist: {{ lvdata.mount.persist | default(false) }}
    - mkmnt: true
    - require:
      - lvm: lvm_lv_create_{{ lv }}
  {%- endif %}
{%-     endif %}

{%-   endfor %}
{%- endif %}

{%- endif %}
