# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Storage meta state

include:
  - {{ slspath }}.disk
  - {{ slspath }}.raid
  - {{ slspath }}.lvm
