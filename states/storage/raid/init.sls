# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Raid meta state

{%- from slspath ~ "/map.jinja" import raid with context %}

{%- if raid.enable is sameas true %}

raid_pkgs:
  pkg.installed:
    - pkgs: {{ raid.pkgs | json }}
    - failhard: True
    - reload_modules: True

{%- for array, conf in raid.get('array', {}) | dictsort %}
raid_present_{{ array }}:
  raid.present:
    - name: /dev/{{ array }}
    - level: {{ conf.level }}
    - devices: {{ conf.devices }}
    - run: True
    - unless:
      - cmd: "grep {{ array }} /proc/mdstat | grep active"
{%- endfor %}

{%- endif %}
