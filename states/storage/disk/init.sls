# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Disk meta state

{% from slspath ~ "/map.jinja" import disk with context %}

{%- if disk.enable is sameas true %}

disk_pkgs:
  pkg.installed:
    - pkgs: {{ disk.pkgs | json }}
    - failhard: True

{%- for name, disk in disk.get('device', {}) | dictsort %}
{%-   set type = "dos" if disk.get('type', 'msdos') == "msdos" else disk.type %}
{%-   set position = { "start": disk.get('startsector', 0) | int } %}
disk_label_{{ name }}:
    cmd.run:
    - name: echo "parted -m -s {{ disk.get('name', name) }} mklabel {{ disk.get('type', 'msdos') }}"
    - unless: "fdisk -l {{ disk.get('name', name) }} | grep -i 'Disklabel type: {{ type }}'"

{%-   for partition in disk.get('partitions', []) %}
{%-     set end_size = position.start %}
{%-     set part = disk.get('name', name) ~ loop.index %}
disk_partition_{{ name }}_{{ loop.index }}:
  module.run:
  {%- if 'module.run' in salt['config.get']('use_superseded', default=[]) %}
  - partition.mkpart:
    - device: {{ disk.name }}
    - part_type: {{ partition.kind }}
    {%- if partition.fstype is defined %}
    - fs_type: {{ partition.fstype }}
    {%- endif %}
    - start: {{ end_size }}{{ disk.unit }}
    - end: {{ end_size + partition.size }}{{ disk.unit }}
  {%- else %}
  - name: partition.mkpart
  - device: {{ disk.name }}
  - part_type: {{ partition.kind }}
  {%- if partition.fstype is defined %}
  - fs_type: {{ partition.fstype }}
  {%- endif %}
  - start: {{ end_size }}{{ disk.unit }}
  - end: {{ end_size + partition.size }}{{ disk.unit }}
  {%- endif %}
  - unless: "blkid {{ disk.name }}{{ loop.index }} {{ disk.name }}p{{ loop.index }}"
  - require:
    - cmd: disk_label_{{ name }}

{%-   if partition.get('raid', false) %}
disk_raid_{{ name }}_{{ loop.index }}:
  module.run:
  {%- if 'module.run' in salt['config.get']('use_superseded', default=[]) %}
  - partition.set:
    - device: {{ disk.name }}
    - minor: {{ loop.index }}
    - flag: raid
    - m_state: 'on'
  {%- else %}
  - name: partition.set
  - device: {{ disk.name }}
  - minor: {{ loop.index }}
  - flag: raid
  - m_state: 'on'
  {%- endif %}
  - unless: "fdisk -l {{ disk.get('name', name) }} | grep -i '{{ part }}' | grep -i raid"
  - require:
    - module: disk_partition_{{ name }}_{{ loop.index }}
{%-   endif %}

{%- if partition.kind != "extended" %}
{%-      do position.update( { "start": position.start + partition.size } ) -%}
{%- endif %}
{%-   endfor %}
{%- endfor %}

{%- endif %}
