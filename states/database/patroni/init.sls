# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Patroni meta state

{%- from slspath ~ "/map.jinja" import patroni with context %}

{%- if patroni.enable is sameas true %}

patroni_pkgs:
  pkg.installed:
    - pkgs: {{ patroni.pkgs | json }}
    - failhard: True

patroni_dir:
  file.directory:
    - name: /var/lib/postgresql
    - user: {{ patroni.user }}
    - group: {{ patroni.group }}
    - require:
      - pkg: patroni_pkgs

{%- if patroni.config is defined %}
patroni_cnf:
  file.managed:
    - name: {{ patroni.config_file }}
    - source: {{ patroni.config_src }}
    - template: jinja
    - makedirs: true
    - require:
      - pkg: patroni_pkgs
    - listen_in:
      - service: patroni_service
{%- endif %}

{%- if patroni.dcs is defined %}
patroni_dcs:
  file.managed:
    - name: {{ patroni.dcs_file }}
    - source: {{ patroni.dcs_src }}
    - template: jinja
    - makedirs: true
{%- endif %}

{%- if patroni.ctl is defined %}
patroni_ctl:
  file.managed:
    - name: /root/.config/patroni/patronictl.yaml
    - contents: |
       {{ patroni.ctl | yaml }}
    - makedirs: true
{%- endif %}

patroni_postgres:
  service.dead:
    - name: postgresql
    - enable: False

{%- if patroni.config is defined %}
patroni_service:
  {%- if patroni.service_enable is sameas true %}
  service.running:
    - name: {{ patroni.service }}
    - enable: {{ patroni.service_enable }}
    - reload: False
    - require:
      - pkg: patroni_pkgs
  {%- else %}
  service.dead:
    - name: {{ patroni.service }}
    - enable: False
  {%- endif %}
{%- endif %}

patroni_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 patroni | grep Active | cut -d ':' -f 2,3,4 | xargs echo patroni"

{% endif %}
