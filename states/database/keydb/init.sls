# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Keydb meta state

{%- from slspath ~ "/map.jinja" import keydb with context %}

{%- if keydb.enable is sameas true %}

keydb_pkgs:
  pkg.installed:
    - pkgs: {{ keydb.pkgs }}
    - failhard: True
    - listen_in:
      - service: keydb_server

keydb_server_cnf:
  file.managed:
    - name: {{ keydb.server_file_path }}
    - source: {{ keydb.config_file_src }}
    - template: jinja
    - user: {{ keydb.user }}
    - group: {{ keydb.group }}
    - mode: 0640
    - listen_in:
      - service: keydb_server

keydb_server:
  {% if keydb.service_enable is sameas true %}
  service.running:
    - name: {{ keydb.service }}
    - enable: {{ keydb.service_enable }}
    - reload: False
    - require:
      - pkg: keydb_pkgs
  {% else %}
  service.dead:
    - name: {{ keydb.service }}
    - enable: False
  {% endif %}

keydb_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 keydb-server | grep Active | cut -d ':' -f 2,3,4 | xargs echo keydb-server"

{% endif %}
