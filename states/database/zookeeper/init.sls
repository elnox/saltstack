# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Zookeeper meta state

{%- from slspath ~ "/map.jinja" import zookeeper with context %}

{%- if zookeeper.enable is sameas true %}

zookeeper_pkgs:
  pkg.installed:
    - pkgs: {{ zookeeper.pkgs }}
    - failhard: True
    - listen_in:
      - service: zookeeper_server

zookeeper_cnf:
  file.managed:
    - name: {{ zookeeper.server_file_path }}
    - source: {{ zookeeper.config_file_src }}
    - template: jinja
    - user: {{ zookeeper.user }}
    - group: {{ zookeeper.group }}
    - mode: 0640
    - listen_in:
      - service: zookeeper_server

{%- for server in zookeeper.get("cluster", []) %}
{%-   if [ server ] | intersect(grains['ipv4']) %}
zookeeper_myid:
  file.managed:
    - name: /etc/zookeeper/conf/myid
    - contents: |
        {{ loop.index }}
{%-   endif %}
{%- endfor %}

zookeeper_server:
  {% if zookeeper.service_enable is sameas true %}
  service.running:
    - name: {{ zookeeper.service }}
    - enable: {{ zookeeper.service_enable }}
    - reload: False
    - require:
      - pkg: zookeeper_pkgs
  {% else %}
  service.dead:
    - name: {{ zookeeper.service }}
    - enable: False
  {% endif %}

zookeeper_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 zookeeper | grep Active | cut -d ':' -f 2,3,4 | xargs echo zookeeper"

{% endif %}
