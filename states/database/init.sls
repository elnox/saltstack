# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Database meta state

include:
  - {{ slspath }}.keydb
  - {{ slspath }}.patroni
  - {{ slspath }}.zookeeper
