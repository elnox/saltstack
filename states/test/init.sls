# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Test state

{%- from slspath ~ "/map.jinja" import test with context %}

test:
  file.managed:
    - name: /tmp/test.yaml
    - template: jinja
    - source: salt://test/debug.jinja
    - context:
      data: {{ test.dataset | json }}
