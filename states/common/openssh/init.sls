# -*- coding: utf-8 -*-
# vim: ft=sls
#
# OpenSSH meta state

{%- from "common/openssh/map.jinja" import openssh with context %}

openssh_pkgs:
  pkg.installed:
    - pkgs: {{ openssh.pkgs | json }}
    - failhard: True

openssh_cnf:
  file.managed:
    - name: {{ openssh.config_path }}
    - source: {{ openssh.config_src }}
    - template: jinja
    - user: {{ openssh.user }}
    - group: {{ openssh.group }}
    - mode: 0644
    - require_in:
      - service: openssh_service
    - watch_in:
      - service: openssh_service

{%- for user, data in openssh.get('auth', {}) | dictsort %}
auth_ssh_dir_{{ user }}:
  file.directory:
    - name: {{ salt['user.info'](user).home ~ "/.ssh" }}
    - user: {{ user }}
    - group: {{ user }}
    - mode: 0700
    - unless: test -d {{ salt['user.info'](user).home ~ "/.ssh" }}

{%-   if data.genkeys is defined %}
{%-     for k, v in data.genkeys | dictsort %}
auth_{{ user }}_genkey_{{ k }}:
  cmd.run:
    - name: ssh-keygen -t {{ v.enc }} {{ "-b " ~ v.bits if v.bits is defined else "" }} -q -N {{ "'" ~ v.pass ~ "'" if v.pass is defined else "''" }} -f {{ salt['user.info'](user).home }}/.ssh/{{ k }}
    - runas: {{ user }}
    - unless: test -f {{ salt['user.info'](user).home }}/.ssh/{{ k }}
    - require:
      - file: auth_ssh_dir_{{ user }}
{%-   endfor %}
{%- endif %}

{%-   if data.authorized is defined %}
{%-     for k, v in data.authorized | dictsort %}
auth_{{ user }}_authorized_{{ k }}:
  ssh_auth.present:
    - user: {{ user }}
    - config: '%h/.ssh/authorized_keys'
    {%- if 'source' in v %}
    - source: {{ v.source }}
    {%- else %}
    - enc: {{ v.enc | default("ssh-rsa") }}
    - comment: {{ v.comment | default(k) }}
    - options: {{ v.options | default([]) }}
    - name:  {{ v.pubkey }}
    {%- endif %}
    - require:
      - file: auth_ssh_dir_{{ user }}
{%-     endfor %}
{%-   endif %}

{%-   if data.unauthorized is defined %}
{%-     for k, v in data.unauthorized | dictsort %}
auth_{{ user }}_unauthorized_{{ k }}:
  ssh_auth.absent:
    - user: {{ user }}
    - config: '%h/.ssh/authorized_keys'
    {%- if 'source' in v %}
    - source: {{ v.source }}
    {%- else %}
    - enc: {{ v.enc | default("ssh-rsa") }}
    - comment: {{ v.comment | default(k) }}
    - options: {{ v.options | default([]) }}
    - name:  {{ v.pubkey }}
    {%- endif %}
    - require:
      - file: auth_ssh_dir_{{ user }}
{%-     endfor %}
{%-   endif %}

{%-   if data.privkeys is defined %}
{%-     for name, source in data.privkeys | dictsort %}
auth_{{ user }}_privkey_{{ name }}:
  file.managed:
    - name: {{ salt['user.info'](user).home ~ "/.ssh/" ~ name }}
    - source: {{ source }}
    - user: {{ user }}
    - mode: 600
    - require:
      - file: auth_ssh_dir_{{ user }}
{%-     endfor %}
{%-   endif %}

{%- endfor %}

openssh_service:
  {%- if openssh.service_enable is sameas true %}
  service.running:
    - name: {{ openssh.service }}
    - enable: {{ openssh.service_enable }}
    - reload: True
    - require:
      - pkg: openssh_pkgs
    {%- if openssh.watch is sameas true %}
    - watch:
      - pkg: openssh_pkgs
      - file: openssh_cnf
    {%- endif %}
  {%- else %}
  service.dead:
    - name: {{ openssh.service }}
    - enable: False
  {%- endif %}
