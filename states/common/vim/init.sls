# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Vim meta state

{%- from "common/vim/map.jinja" import vim with context %}

vim_pkgs:
  pkg.installed:
    - pkgs: {{ vim.pkgs | json }}
    - failhard: True

{%- if vim.managed_vimrc is sameas true %}
vim_config:
  file.managed:
    - name: {{ vim.vimrc_path }}
    - source: {{ vim.vimrc_src }}
    - template: jinja
    - user: {{ vim.user }}
    - group: {{ vim.group }}
    - mode: 644
    - makedirs: True
{%- endif %}

{%- if vim.allow_localrc is sameas true %}
vim_local:
  file.managed:
    - name: {{ vim.vimloc_path }}
    - source: {{ vim.vimrc_local }}
    - template: jinja
    - user: {{ vim.user }}
    - group: {{ vim.group }}
    - mode: 644
{%- endif %}

{%- if vim.allow_vimplug is sameas true %}
vim_dir:
  file.directory:
    - name: {{ vim.autoload_path }}
    - user: {{ vim.user }}
    - group: {{ vim.group }}
    - mode: 755
    - makedirs: True

vim_autoload:
  file.managed:
    - name: {{ vim.autoload_path }}/plug.vim
    - source: {{ vim.vimrc_autoload }}
    - user: {{ vim.user }}
    - group: {{ vim.group }}

vim_plug:
  file.managed:
    - name: {{ vim.vimplug_path }}
    - source: {{ vim.vimrc_plug }}
    - user: {{ vim.user }}
    - group: {{ vim.group }}
    - mode: 644
    - makedirs: True
{%- endif %}
