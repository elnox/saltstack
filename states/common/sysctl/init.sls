# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Sysctl management states

{%- from "common/sysctl/map.jinja" import sysctl with context %}

sysctl_pkg:
  pkg.installed:
    - name: {{ sysctl.pkg }}
    - failhard: True

{%- for key, value in sysctl.settings | dictsort %}
sysctl_present_{{ key }}:
  environ.setenv:
    - name: PATH
    - value: "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
  sysctl.present:
    - name: {{ key }}
    - value: {{ value }}
    - config: {{ sysctl.path }}
{%- endfor %}
