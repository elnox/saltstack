# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Ntp management states

{%- from "common/ntp/map.jinja" import ntp with context %}

ntp_pkgs:
  pkg.installed:
    - pkgs: {{ ntp.pkgs | json }}
    - failhard: True

ntp_cnf:
  file.managed:
    - name: {{ ntp.config_path }}
    - template: jinja
    - source: {{ ntp.config_src }}
    - defaults:
        drift: '{{ ntp.drift }}'

ntp_date:
  file.managed:
    - name: {{ ntp.client_path }}
    - source: {{ ntp.client_src }}

ntp_server:
  {%- if ntp.enable is sameas true %}
  service.running:
    - name: {{ ntp.service }}
    - enable: {{ ntp.enable }}
    - reload: False
    - require:
      - pkg: ntp_pkgs
    {%- if ntp.watch is sameas true %}
    - watch:
      - pkg: ntp_pkgs
      - file: ntp_cnf
    {%- endif %}
  {%- else %}
  service.dead:
    - name: {{ ntp.service }}
    - enable: False
  {%- endif %}

{%- if ntp.service_enable is sameas true %}
ntp_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 ntp | grep Active | cut -d ':' -f 2,3,4 | xargs echo ntp"
{%- endif %}
