# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Resolver meta state

{%- from "common/resolver/map.jinja" import resolver with context %}

{%- if resolver.enable is sameas true %}
resolver_pkgs:
  {%- if resolver.enabled is sameas True %}
  pkg.installed:
    - pkgs: {{ resolver.pkgs | json }}
    - failhard: True
  {%- else %}
  pkg.purged:
    - pkgs: {{ resolver.pkgs | json }}
    - failhard: True
  {%- endif %}

resolver_conf:
  file.managed:
    {%- if resolver.enabled %}
    - name: {{ resolver.config_path }}
    {%- else %}
    - name: /etc/resolv.conf
    - follow_symlinks: False
    {%- endif %}
    - user: {{ resolver.user }}
    - group: {{ resolver.group }}
    - mode: '0644'
    - source: {{ resolver.config_src }}
    - template: jinja
    - defaults:
        nameservers: {{ resolver.nameservers }}
        searchpaths: {{ resolver.searchpaths }}
        options: {{ resolver.options }}
        domain: {{ resolver.domain }}

{%- if resolver.enabled is sameas True %}
resolver_symlink:
  file.symlink:
    - name: /etc/resolv.conf
    - target: {{ resolver.config_path }}
    - force: True
  cmd.run:
    - name: resolvconf -u
    - onchanges:
      - file: resolver_conf
{%- endif %}

{%- if resolver.service_managed and salt['file.file_exists'](resolver.service_conf) %}
resolver_service:
  ini.options_present:
    - name: {{ resolver.service_conf }}
    - separator: '='
    - strict: False
    - sections:
        main:
          dns: {{ resolver.service_dns }}
    - onlyif: systemctl is-enabled {{ resolver.service }}
    - require:
      - file: resolver_conf
    - watch_in:
      - service: resolver_service
  service.running:
    - name: {{ resolver.service }}
    - enable: True
{%- endif %}
{%- endif %}
