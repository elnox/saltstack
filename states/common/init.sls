# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Common meta state

include:
  - {{ slspath }}.apt
  - {{ slspath }}.bash
  - {{ slspath }}.cron
  - {{ slspath }}.debian
  - {{ slspath }}.hostname
  - {{ slspath }}.locale
  - {{ slspath }}.logrotate
  - {{ slspath }}.network
  - {{ slspath }}.ntp
  - {{ slspath }}.openssh
  - {{ slspath }}.repos
  - {{ slspath }}.resolver
  - {{ slspath }}.sysctl
  - {{ slspath }}.syslog
  - {{ slspath }}.timezone
  - {{ slspath }}.vim
