# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Debian meta state

{%- from "common/debian/map.jinja" import debian with context %}

{%- set packages = debian.get('packages', {}) %}

{%- if packages.present is defined %}
debian_pkgs_present:
  pkg.installed:
    - pkgs: {{ packages.present | json }}
    - failhard: True
    - reload_modules: True
{%- endif %}

{%- if packages.purged is defined %}
debian_pkgs_purged:
  pkg.purged:
    - pkgs: {{ packages.purged | json }}
{% endif %}

{%- for service in debian.get('services', {}) %}
debian_service_{{ service }}:
  service.running:
    - name: {{ service }}
    - enable: true
    - reload: True
{%- endfor %}

{%- if debian.limits is defined and debian.limits | length %}
debian_limits:
  file.managed:
    - name: /etc/security/limits.conf
    - user: root
    - group: root
    - template: jinja
    - source: {{ debian.security_limits }}
{%- endif %}

{%- if debian.sudoers is sameas true %}
debian_sudoers:
  file.managed:
    - name: /etc/sudoers
    - source: {{ debian.sudoers_path }}
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
{%- endif %}

{%- if debian.journald is defined %}
debian_journald:
  file.managed:
    - name: /etc/systemd/journald.conf
    - source: {{ debian.journald_path }}
    - user: root
    - group: root
    - mode: '0755'
    - template: jinja

debian_restart_journald:
  service.running:
    - name: systemd-journald
    - enable: true
    - watch:
      - file: debian_journald
{%- endif %}

{%- for symlink in debian.symlinks %}
{%-   set filename = salt['file.basename'](symlink) %}
debian_symlink_{{ filename }}:
  file.symlink:
    - name: /usr/local/bin/{{ filename }}
    - target: {{ symlink }}
    - force: true
    - onlyif: test -f {{ symlink }}
{%- endfor %}

{%- for file, opts in debian.get('files', {}) | dictsort %}
debian_file_{{ file }}:
  file.managed:
    - name: {{ opts.path }}/{{ file }}
    {%- if opts.source is defined %}
    - source: {{ opts.source }}
    - skip_verify: true
    {%- else %}
    - source: {{ debian.alternative_path }}/{{ opts.path }}/{{ file }}
    {%- endif %}
    - user: {{ opts.user | default("root") }}
    - group: {{ opts.group | default("root") }}
    - mode: {{ opts.mode | default("644") }}
{%- endfor %}

{%- for dir, opts in debian.get('directories', {}) | dictsort %}
debian_{{ dir }}:
  file.directory:
    - name: {{ opts.path }}
    - user: {{ opts.user }}
    - group: {{ opts.group }}
    - mode: 755
{%- endfor %}

{%- for file in debian.get('remove', []) %}
debian_remove{{ file | replace('/', '-') }}:
  file.absent:
    - name: {{ file }}
{%- endfor %}
