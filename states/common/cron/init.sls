# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Cron meta state

{%- from "common/cron/map.jinja" import cron with context %}

cron_pkg:
  pkg.installed:
    - name: {{ cron.pkg }}
    - failhard: True

cron_cnf:
  file.managed:
    - name: {{ cron.config_path }}
    - source: {{ cron.config_src }}
    - template: jinja
    - user: {{ cron.user }}
    - group: {{ cron.group }}
    - mode: {{ cron.mode }}
    - watch_in:
      - service: cron_service

{%- for user, env in cron.get('env', {}) | dictsort %}
{%-   for data in env %}
cron-{{ user }}-env-{{ data.name | lower }}:
  environ.setenv:
    - name: PATH
    - value: "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
  {%- if data.present is sameas true %}
  cron.env_present:
    - name: {{ data.name }}
    - user: {{ user }}
    - value: {{ data.value }}
  {%- else %}
  cron.env_absent:
    - name: {{ data.name }}
    - user: {{ user }}
  {%- endif %}
{%-   endfor %}
{%- endfor %}

{%- for task, opts in cron.tasks | dictsort %}
cron_{{ task }}:
  cron.{{ opts.type | default('present') }}:
    - name: '{{ opts.name }}'
    - user: {{ opts.user | default('root') }}
    {%- if 'minute' in opts %}
    - minute: '{{ opts.minute }}'
    {%- endif %}
    {%- if 'hour' in opts %}
    - hour: '{{ opts.hour }}'
    {%- endif %}
    {%- if 'daymonth' in opts %}
    - daymonth: '{{ opts.daymonth }}'
    {%- endif %}
    {%- if 'month' in opts %}
    - month: '{{ opts.month }}'
    {%- endif %}
    {%- if 'dayweek' in opts %}
    - dayweek: '{{ opts.dayweek }}'
    {%- endif %}
    {%- if 'commented' in opts and opts.commented %}
    - commented: True
    {%- endif %}
    {%- if 'special' in opts %}
    - special: '{{ opts.special }}'
    {%- endif %}
    - identifier: '{{ task }}'
    {%- if 'comment' in opts %}
    - comment: '{{ opts.comment }}'
    {%- endif %}
{% endfor %}

cron_service:
  {%- if cron.service_enable is sameas true %}
  service.running:
    - name: {{ cron.service }}
    - enable: {{ cron.service_enable }}
    - require:
      - pkg: cron_pkg
    {%- if cron.watch is sameas true %}
    - watch:
      - pkg: cron_pkg
      - file: cron_cnf
    {%- endif %}
  {%- else %}
  service.dead:
    - name: {{ cron.service }}
    - enable: False
  {%- endif %}

{%- if cron.service_enable is sameas true %}
cron_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 cron | grep Active | cut -d ':' -f 2,3,4 | xargs echo cron"
{%- endif %}
