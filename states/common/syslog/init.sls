# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Syslog-ng meta state

{%- from "common/syslog/map.jinja" import syslog with context %}

syslog_pkg:
  pkg.installed:
    - name: {{ syslog.pkg }}
    - failhard: True

syslog_cnf:
  file.managed:
    - name: {{ syslog.config_path }}
    - source: {{ syslog.config_src }}
    - template: jinja
    - user: {{ syslog.user }}
    - group: {{ syslog.group }}
    - mode: {{ syslog.mode }}
    - watch_in:
      - service: syslog_service

syslog_directory:
  file.directory:
    - name: /var/log/syslog
    - user: {{ syslog.user }}
    - group: {{ syslog.group }}
    - makedirs: True
    - force: True

syslog_service:
  {%- if syslog.service_enable is sameas true %}
  service.running:
    - name: {{ syslog.service }}
    - enable: {{ syslog.service_enable }}
    - require:
      - pkg: syslog_pkg
    {%- if syslog.watch is sameas true %}
    - watch:
      - pkg: syslog_pkg
      - file: syslog_cnf
    {%- endif %}
  {%- else %}
  service.dead:
    - name: {{ syslog.service }}
    - enable: False
  {%- endif %}

{%- if syslog.service_enable is sameas true %}
syslog_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 syslog-ng | grep Active | cut -d ':' -f 2,3,4 | xargs echo syslog-ng"
{%- endif %}
