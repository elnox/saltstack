# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Network management states

{%- from "common/network/map.jinja" import network with context %}

{%- if network.enable is sameas true %}

{%- for iface, routes in network.routes | dictsort %}
network_routes_{{ iface }}:
  network.routes:
    - name: {{ iface }}
    - routes:
  {%- for net in routes %}
      - name: {{ net.name }}
        ipaddr: {{ net.ipaddr }}
        netmask: {{ net.netmask }}
        {%- if net.gateway is defined %}
        gateway: {{ net.gateway }}
        {%- endif %}
  {%- endfor %}
{%- endfor %}

{%- if network.interfaces is defined %}
network_interfaces:
  file.managed:
    - name: {{ network.interfaces_path }}
    - user: root
    - group: root
    - template: jinja
    - source: {{ network.interfaces_src }}

network_service:
  {%- if network.service_enable is sameas true %}
  service.running:
    - name: {{ network.service }}
    - enable: {{ network.service_enable }}
    - reload: False
    {%- if network.watch is sameas true %}
    - watch:
      - file: network_interfaces
    {%- endif %}
  {%- else %}
  service.dead:
    - name: {{ network.service }}
    - enable: False
  {%- endif %}
{%- endif %}

{%- if network.service_enable is sameas true %}
network_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 networking | grep Active | cut -d ':' -f 2,3,4 | xargs echo networking"
{%- endif %}

{%- endif %}
