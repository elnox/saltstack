# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Bash meta state

{%- from "common/bash/map.jinja" import bash with context %}

bash_pkgs:
  pkg.installed:
    - pkgs: {{ bash.pkgs | json }}
    - failhard: True

{%- for user in bash.users %}
{%-   set current = salt.user.info(user) -%}
{%-   set home    = current.get('home', "/home/%s" % user) -%}
{%-   set source  = bash.alt_path + '/' + user %}
bash_profile_{{ user }}:
  file.managed:
    - name: {{ home }}/.bash_profile
    - user: {{ user }}
    - group: {{ user }}
    - mode: 744
    - template: jinja
    - source:
      - {{ source }}/profile
      - {{ bash.src_path }}/profile.jinja

bash_rc_{{ user }}:
  file.managed:
    - name: {{ home }}/.bashrc
    - user: {{ user }}
    - group: {{ user }}
    - mode: 644
    - template: jinja
    - source:
      - {{ source }}/bashrc
      - {{ bash.src_path }}/bashrc.jinja

bash_alias_{{ user }}:
  file.managed:
    - name: {{ home }}/.bash_aliases
    - user: {{ user }}
    - group: {{ user }}
    - mode: 744
    - template: jinja
    - source:
      - {{ source }}/aliases
      - {{ bash.src_path }}/aliases.jinja
    - unless: test -d {{ home }}/.bash.d
{%- endfor %}

bash_systemctl:
  file.touch:
    - name: /var/spool/.systemctl_status
    - unless: test -f /var/spool/.systemctl_status
