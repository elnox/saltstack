# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Kubernetes certificate state

{% from "kubernetes/map.jinja" import kubernetes with context %}

{%- set k8s = kubernetes.kube %}
{%- set auth = kubernetes.k8scerts %}

{%- if auth.enable is sameas true %}

k8s_ca_pubkey:
  x509.pem_managed:
    - name: {{ k8s.cert_dir }}/ca.crt
    - text: {{ salt['mine.get'](auth.trust,'x509.get_pem_entries')[auth.trust]['/etc/pki/ca.crt'] | replace('\n','') }}
  file.managed:
    - name: {{ k8s.cert_dir }}/ca.crt
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}

k8s_ca_privkey:
  x509.pem_managed:
    - name: {{ k8s.cert_dir }}/ca.key
    - text: {{ salt['mine.get'](auth.trust,'x509.get_pem_entries')[auth.trust]['/etc/pki/ca.key'] | replace('\n','') }}
  file.managed:
    - name: {{ k8s.cert_dir }}/ca.key
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - mode: 600

{%- for cert, data in auth.certificates | dictsort %}
k8s_key_{{ cert }}:
  x509.private_key_managed:
    - name: {{ k8s.cert_dir }}/{{ cert }}.key
    - bits: {{ data.get('bits', 4096) }}
  file.managed:
    - name: {{ k8s.cert_dir }}/{{ cert }}.key
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - mode: 600

k8s_cert_{{ cert }}:
  x509.certificate_managed:
    - name: {{ k8s.cert_dir }}/{{ cert }}.crt
    - ca_server: "kali.drakonix.loc"
    - signing_policy: internal
    - public_key: {{ k8s.cert_dir }}/{{ cert }}.key
    - CN: {{ data.get('cn', grains["id"]) }}
    - append_certs:
        - /usr/local/share/ca-certificates/ca.crt
    - subjectAltName: '{{ data.get('san', ["RID:1.2.3.4"]) | join(",") }}'
    - days_valid: {{ data.get('validity', 30) }}
    - days_remaining: {{ data.get('remain', 10) }}
    - check_cmd:
        - 'openssl verify {{ k8s.cert_dir }}/{{ cert }}.crt'
  file.managed:
    - name: {{ k8s.cert_dir }}/{{ cert }}.crt
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
{%- endfor %}

{%- endif %}
