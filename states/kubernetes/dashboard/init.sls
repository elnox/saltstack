# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Dashboard state

{%- from "kubernetes/map.jinja" import kubernetes with context %}

{%- set k8s = kubernetes.kube %}
{%- set dash = kubernetes.dashboard %}

{%- if dash.enable is sameas true %}

k8s_dashboard_sources:
  archive.extracted:
    - name: {{ k8s.home }}/
    - source: {{ dash.binary }}
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - skip_verify: true
    - makedirs: true
    - listen_in:
      - service: {{ dash.service }}

k8s_dashboard_config:
 file.managed:
    - name: {{ k8s.home }}/{{ dash.config.path }}
    - source: {{ dash.config.src }}
    - template: jinja
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - mode: 640
    - listen_in:
      - service: {{ dash.service }}

k8s_dashboard_systemd:
  file.managed:
    - name: {{ k8s.systemd_lib }}/{{ dash.service }}.service
    - source: {{ dash.systemd }}
    - template: jinja
    - mode: 644
    - listen_in:
      - service: {{ dash.service }}

k8s_dashboard_service:
  {%- if dash.service_enable is sameas true %}
  service.running:
    - name: {{ dash.service }}
    - enable: {{ dash.service_enable }}
    - reload: False
  {%- else %}
  service.dead:
    - name: {{ dash.service }}
    - enable: False
  {%- endif %}

k8s_dashboard_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 dashboard | grep Active | cut -d ':' -f 2,3,4 | xargs echo dashboard"

{%- endif %}
