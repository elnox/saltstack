# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Apiserver state

{%- from "kubernetes/map.jinja" import kubernetes with context %}

{%- set k8s = kubernetes.kube %}
{%- set api = kubernetes.apiserver %}

{%- if api.enable is sameas true %}

k8s_apiserver_audit:
 file.managed:
    - name: {{ k8s.home }}/{{ api.audit.path }}
    - source: {{ api.audit.src }}
    - template: jinja
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - mode: 640

k8s_apiserver_encryption:
 file.managed:
    - name: {{ k8s.home }}/{{ api.encryption.path }}
    - source: {{ api.encryption.src }}
    - template: jinja
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - mode: 640

k8s_apiserver_systemd:
  file.managed:
    - name: {{ k8s.systemd_lib }}/{{ api.service }}.service
    - source: {{ api.systemd }}
    - template: jinja
    - mode: 644
    - listen_in:
      - service: {{ api.service }}

k8s_apiserver_service:
  {%- if api.service_enable is sameas true %}
  service.running:
    - name: {{ api.service }}
    - enable: {{ api.service_enable }}
    - reload: False
  {%- else %}
  service.dead:
    - name: {{ api.service }}
    - enable: False
  {%- endif %}

k8s_apiserver_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 apiserver | grep Active | cut -d ':' -f 2,3,4 | xargs echo apiserver"

{%- endif %}
