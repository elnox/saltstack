# -*- coding: utf-8 -*-
# vim: ft=sls
#
# k8s tooling meta state

{% from "kubernetes/map.jinja" import kubernetes with context %}
{% set tool = kubernetes.tooling %}

{% if tool.enable is sameas true %}

kube_config:
 file.managed:
    - name: {{ tool.kubeconfig.path }}
    - source: {{ tool.kubeconfig.src }}
    - template: jinja
    - makedirs: true

stern_binary:
  file.managed:
    - name: {{ tool.stern.dest }}
    - source: {{ tool.stern.url }}
    - source_hash: {{ tool.stern.hash }}
    - user: {{ tool.stern.user }}
    - group: {{ tool.stern.group }}
    - mode: {{ tool.stern.mode }}
    - unless:
      - fun: file.file_exists
        path: {{ tool.stern.dest }}

{% endif %}
