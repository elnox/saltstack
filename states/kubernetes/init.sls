# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Kubernetes master state

include:
  - {{ slspath }}.kube
  - {{ slspath }}.k8scerts
  - {{ slspath }}.multus
  - {{ slspath }}.apiserver
  - {{ slspath }}.controller
  - {{ slspath }}.scheduler
  - {{ slspath }}.kubelet
  - {{ slspath }}.proxy
  - {{ slspath }}.dashboard
  - {{ slspath }}.tooling
