# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Proxy state

{%- from "kubernetes/map.jinja" import kubernetes with context %}

{%- set k8s = kubernetes.kube %}
{%- set proxy = kubernetes.proxy %}

{%- if proxy.enable is sameas true %}

k8s_proxy_pkgs:
  pkg.installed:
    - pkgs: {{ proxy.pkgs | json }}
    - failhard: True
  kmod.present:
    - mods: {{ proxy.kmods | json }}
    - persist: True
    - failhard: True

k8s_proxy_config:
 file.managed:
    - name: {{ k8s.home }}/{{ proxy.config.path }}
    - source: {{ proxy.config.src }}
    - template: jinja
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - mode: 640
    - listen_in:
      - service: {{ proxy.service }}

k8s_proxy_kubeconfig:
 file.managed:
    - name: {{ k8s.home }}/{{ proxy.kubeconfig.path }}
    - source: {{ proxy.kubeconfig.src }}
    - template: jinja
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - mode: 640

k8s_proxy_systemd:
  file.managed:
    - name: {{ k8s.systemd_lib }}/{{ proxy.service }}.service
    - source: {{ proxy.systemd }}
    - template: jinja
    - mode: 644
    - listen_in:
      - service: {{ proxy.service }}

k8s_proxy_service:
  {%- if proxy.service_enable is sameas true %}
  service.running:
    - name: {{ proxy.service }}
    - enable: {{ proxy.service_enable }}
    - reload: False
  {%- else %}
  service.dead:
    - name: {{ proxy.service }}
    - enable: False
  {%- endif %}

k8s_proxy_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 proxy | grep Active | cut -d ':' -f 2,3,4 | xargs echo proxy"

{%- endif %}
