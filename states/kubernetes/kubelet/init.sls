# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Kubelet state

{% from "kubernetes/map.jinja" import kubernetes with context %}

{% set k8s = kubernetes.kube %}
{% set kblt = kubernetes.kubelet %}

{% if kblt.enable is sameas true %}

k8s_kubelet_pkgs:
  pkg.installed:
    - pkgs: {{ kblt.pkgs | json }}
    - failhard: True
  kmod.present:
    - mods: {{ kblt.kmods | json }}
    - persist: True
    - failhard: True

k8s_kubelet_group:
  group.present:
    - name: docker
    - system: True
    - members: [ {{ k8s.user }} ]

k8s_kubelet_swapoff:
  file.line:
    - name: /etc/fstab
    - match: swap
    - mode: delete
  cmd.run:
    - name: swapoff -a
    - onchanges:
      - file: k8s_kubelet_swapoff

k8s_kubelet_config:
 file.managed:
    - name: {{ k8s.home }}/{{ kblt.config.path }}
    - source: {{ kblt.config.src }}
    - template: jinja
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - mode: 640
    - listen_in:
      - service: {{ kblt.service }}

k8s_kubelet_kubeconfig:
 file.managed:
    - name: {{ k8s.home }}/{{ kblt.kubeconfig.path }}
    - source: {{ kblt.kubeconfig.src }}
    - template: jinja
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - mode: 640

k8s_kubelet_symlink:
  file.symlink:
    - name: /usr/local/bin/kubelet
    - target: {{ k8s.home }}/server/bin/kubelet

k8s_kubelet_systemd:
  file.managed:
    - name: {{ k8s.systemd_lib }}/{{ kblt.service }}.service
    - source: {{ kblt.systemd }}
    - template: jinja
    - mode: 644
    - listen_in:
      - service: {{ kblt.service }}

k8s_kubelet_service:
  {% if kblt.service_enable is sameas true %}
  service.running:
    - name: {{ kblt.service }}
    - enable: {{ kblt.service_enable }}
    - reload: False
  {% else %}
  service.dead:
    - name: {{ kblt.service }}
    - enable: False
  {% endif %}

k8s_kubelet_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 kubelet | grep Active | cut -d ':' -f 2,3,4 | xargs echo kubelet"

{% endif %}
