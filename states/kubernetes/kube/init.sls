# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Kubernetes kube state

{%- from "kubernetes/map.jinja" import kubernetes with context %}

{%- set k8s = kubernetes.kube %}

{%- if k8s.enable is sameas true %}

k8s_user:
  user.present:
    - name: {{ k8s.user }}
    - system: True
    - shell: {{ k8s.shell }}
    - usergroup: True
    - createhome: True
    - home: {{ k8s.home }}

k8s_source_extract:
  archive.extracted:
    - name: {{ k8s.tmp_path }}
    - source: {{ k8s.src.mirror }}/v{{ k8s.src.version }}/kubernetes.tar.gz
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - skip_verify: true
    - keep_source: false
    - makedirs: true
    - unless:
      - fun: file.file_exists
        path: {{ k8s.home }}/v{{ k8s.src.version }}

k8s_server_extract:
  cmd.run:
    - name: 'KUBERNETES_SKIP_CONFIRM=1 ./cluster/get-kube-binaries.sh'
    - cwd: {{ k8s.tmp_path }}/kubernetes
    - onchanges:
      - archive: k8s_source_extract
  archive.extracted:
    - name: {{ k8s.tmp_path }}
    - source: {{ k8s.tmp_path }}/kubernetes/server/kubernetes-server-linux-amd64.tar.gz
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - skip_verify: true
    - makedirs: true
    - unless:
      - fun: file.file_exists
        path: {{ k8s.home }}/v{{ k8s.src.version }}
  rsync.synchronized:
    - name: {{ k8s.home }}
    - source: {{ k8s.tmp_path }}/kubernetes/
    - force: true
    - onchanges:
      - archive: k8s_server_extract
  file.touch:
    - name: {{ k8s.home }}/v{{ k8s.src.version }}
    - onchanges:
      - rsync: k8s_server_extract

k8s_cni_extract:
  archive.extracted:
    - name: {{ k8s.home }}/addons/cni
    - source: {{ k8s.cni.mirror }}/v{{ k8s.cni.version }}/cni-plugins-linux-amd64-v{{ k8s.cni.version }}.tgz
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - skip_verify: true
    - makedirs: true

k8s_fix_client:
  file.symlink:
    - name: {{ k8s.home }}/client/bin
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - target: {{ k8s.home }}/platforms/linux/amd64

k8s_kubectl_symlink:
  file.symlink:
    - name: /usr/local/bin/kubectl
    - target: {{ k8s.home }}/client/bin/kubectl

{% for dir in k8s.directories %}
k8s_directory_{{ dir }}:
  file.directory:
    - name: {{ k8s.home }}/{{ dir }}
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - dir_mode: 750
    - file_mode: 644
    - makedirs: True
{% endfor %}

{%- endif %}
