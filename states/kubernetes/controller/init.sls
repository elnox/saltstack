# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Controller state

{%- from "kubernetes/map.jinja" import kubernetes with context %}

{%- set k8s = kubernetes.kube %}
{%- set ctrl = kubernetes.controller %}

{%- if ctrl.enable is sameas true %}

k8s_controller_config:
 file.managed:
    - name: {{ k8s.home }}/{{ ctrl.config.path }}
    - source: {{ ctrl.config.src }}
    - template: jinja
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - mode: 640
    - listen_in:
      - service: {{ ctrl.service }}

k8s_controller_systemd:
  file.managed:
    - name: {{ k8s.systemd_lib }}/{{ ctrl.service }}.service
    - source: {{ ctrl.systemd }}
    - template: jinja
    - mode: 644
    - listen_in:
      - service: {{ ctrl.service }}

k8s_controller_service:
  {%- if ctrl.service_enable is sameas true %}
  service.running:
    - name: {{ ctrl.service }}
    - enable: {{ ctrl.service_enable }}
    - reload: False
  {%- else %}
  service.dead:
    - name: {{ ctrl.service }}
    - enable: False
  {%- endif %}

k8s_controller_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 controller | grep Active | cut -d ':' -f 2,3,4 | xargs echo controller"

{%- endif %}
