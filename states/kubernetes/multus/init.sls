# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Multus state

{%- from "kubernetes/map.jinja" import kubernetes with context %}

{%- set k8s = kubernetes.kube %}
{%- set multus = kubernetes.multus %}

{%- if multus.enable is sameas true %}

k8s_multus_binary:
  archive.extracted:
    - name: {{ k8s.tmp_path }}
    - source: {{ multus.mirror }}/v{{ multus.version }}/{{ "multus-cni_" ~ multus.version ~ "_linux_amd64.tar.gz" }}
    - mode: 755
    - skip_verify: true
    - keep_source: true
    - makedirs: true
    - unless: test -f {{ k8s.home }}/v{{ multus.version }}
  file.rename:
    - name: {{ k8s.home }}/addons/cni/multus
    - source: {{ k8s.tmp_path }}/multus-cni_{{ multus.version }}_linux_amd64/multus-cni
    - makedirs: true
    - force: true
    - unless: test -f {{ k8s.home }}/v{{ multus.version }}

k8s_multus_deployed:
  file.touch:
    - name: {{ k8s.home }}/v{{ multus.version }}
    - unless: test -f {{ k8s.home }}/v{{ multus.version }}

k8s_multus_config:
 file.managed:
    - name: {{ k8s.home }}/{{ multus.config.path }}
    - source: {{ multus.config.src }}
    - template: jinja
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - mode: 640

k8s_multus_kubeconfig:
 file.managed:
    - name: {{ k8s.home }}/{{ multus.kubeconfig.path }}
    - source: {{ multus.kubeconfig.src }}
    - template: jinja
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - mode: 640

{%- endif %}
