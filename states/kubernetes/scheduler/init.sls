# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Scheduler state

{%- from "kubernetes/map.jinja" import kubernetes with context %}

{%- set k8s = kubernetes.kube %}
{%- set sched = kubernetes.scheduler %}

{%- if sched.enable is sameas true %}

k8s_scheduler_config:
 file.managed:
    - name: {{ k8s.home }}/{{ sched.config.path }}
    - source: {{ sched.config.src }}
    - template: jinja
    - user: {{ k8s.user }}
    - group: {{ k8s.group }}
    - mode: 640
    - listen_in:
      - service: {{ sched.service }}

k8s_scheduler_systemd:
  file.managed:
    - name: {{ k8s.systemd_lib }}/{{ sched.service }}.service
    - source: {{ sched.systemd }}
    - template: jinja
    - mode: 644
    - listen_in:
      - service: {{ sched.service }}

k8s_scheduler_service:
  {%- if sched.service_enable is sameas true %}
  service.running:
    - name: {{ sched.service }}
    - enable: {{ sched.service_enable }}
    - reload: False
  {%- else %}
  service.dead:
    - name: {{ sched.service }}
    - enable: False
  {%- endif %}

k8s_scheduler_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 scheduler | grep Active | cut -d ':' -f 2,3,4 | xargs echo scheduler"

{%- endif %}
