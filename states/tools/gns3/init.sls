# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Gns3 meta state

{%- from slspath ~ "/map.jinja" import gns3 with context %}

{%- if gns3.enable is sameas true %}

{%- if gns3.server_enable is sameas true %}
gns3_server_pkgs:
  pkg.installed:
    - pkgs: {{ gns3.server_pkgs | json }}
    - failhard: True
    - reload_modules: True

gns3_server_pip_pkg:
  pip.installed:
    - name: gns3-server
    - bin_env: '/usr/bin/pip3'
    - require:
      - pkg: gns3_server_pkgs

gns3_server_ubuntu_dynamips:
  pkg.installed:
    - name: dynamips
    - version: 0.2.22-1~bionic1
    - require:
      - pkg: gns3_server_pkgs

gns3_server_ubuntu_ubridge:
  pkg.installed:
    - name: ubridge
    - version: 0.9.18-1~bionic2
    - require:
      - pkg: gns3_server_pkgs

gns3_server_ubuntu_vpcs:
  pkg.installed:
    - name: vpcs
    - version: 1:0.8.2-1~bionic2
    - require:
      - pkg: gns3_server_pkgs
{%- endif %}

{%- if gns3.client_enable is sameas true %}
gns3_client_pkgs:
  pkg.installed:
    - pkgs: {{ gns3.client_pkgs | json }}
    - failhard: True
    - reload_modules: True

gns3_client_pip_pkg:
  pip.installed:
    - name: gns3-gui
    - bin_env: '/usr/bin/pip3'
    - require:
      - pkg: gns3_client_pkgs
{%- endif %}

{%- if gns3.remote is sameas true %}
gns3_config_dir:
  file.directory:
    - name: {{ gns3.config_path }}
    - user: {{ gns3.user }}
    - group: {{ gns3.group }}
    - makedirs: True

gns3_config:
  file.managed:
    - name: {{ gns3.config_path }}/{{ gns3.config_file }}
    - source: salt://tools/files/gns3.jinja
    - listen_in:
      - service: gns3_service

gns3_systemd:
  file.managed:
    - name: {{ gns3.systemd_path }}
    - source: salt://tools/files/gns3.systemd
    - listen_in:
      - service: gns3_service

gns3_service:
  {%- if gns3.server_enable is sameas true %}
  service.running:
    - name: {{ gns3.service }}
    - enable: {{ gns3.server_enable }}
  {%- else %}
  service.dead:
    - name: {{ gns3.service }}
    - enable: False
  {%- endif %}

gsn3_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 gns3 | grep Active | cut -d ':' -f 2,3,4 | xargs echo gns3"
{%- endif %}

{%- endif %}
