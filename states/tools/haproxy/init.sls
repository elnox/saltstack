# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Haproxy meta state

{%- from slspath ~ "/map.jinja" import haproxy with context %}

{%- if haproxy.enable is sameas true %}

haproxy_pkgs:
  pkg.installed:
    - pkgs: {{ haproxy.pkgs | json }}
    - failhard: True

haproxy_chroot:
  file.directory:
    - name: {{ haproxy.chroot_path }}
    - user: {{ haproxy.user }}
    - group: {{ haproxy.group }}

haproxy_modules:
  file.directory:
    - name: {{ haproxy.module_path }}
    - user: {{ haproxy.user }}
    - group: {{ haproxy.group }}

haproxy_ssl:
  file.directory:
    - name: {{ haproxy.certificate_path }}
    - user: {{ haproxy.user }}
    - group: {{ haproxy.group }}

haproxy_cnf:
  file.managed:
    - name: {{ haproxy.config_path }}
    - source: {{ haproxy.config_src }}
    - template: jinja
    - user: {{ haproxy.user }}
    - group: {{ haproxy.group }}
    - mode: 644
    - listen_in:
      - service: haproxy_service

{%- for name, path in haproxy.scripts | dictsort %}
haproxy_script_{{ name }}:
  file.managed:
    - name: {{ path ~ "/" ~ name }}
    - source: {{ haproxy.alt_path ~ "/" ~ name }}
    - user: {{ haproxy.user }}
    - group: {{ haproxy.group }}
    - mode: 644
    - listen_in:
      - service: haproxy_service
{%- endfor %}

{%- for cert, data in haproxy.certificates | dictsort %}
haproxy_key_{{ cert }}:
  x509.private_key_managed:
    - name: /etc/haproxy/ssl.d/{{ cert }}.crt.key
    - bits: {{ data.get('bits', 4096) }}
  file.managed:
    - name: /etc/pki/{{ cert }}.key
    - user: {{ "root" }}
    - group: {{ "root" }}
    - mode: 600

haproxy_cert_{{ cert }}:
  x509.certificate_managed:
    - name: /etc/haproxy/ssl.d/{{ cert }}.crt
    - ca_server: "kali.drakonix.loc"
    - signing_policy: internal
    - public_key: /etc/haproxy/ssl.d/{{ cert }}.crt.key
    - CN: {{ data.get('cn', grains["id"]) }}
    - append_certs:
        - /usr/local/share/ca-certificates/ca.crt
    - subjectAltName: '{{ data.get('san', ["RID:1.2.3.4"]) | join(",") }}'
    - days_valid: {{ data.get('validity', 30) }}
    - days_remaining: {{ data.get('remain', 10) }}
    - check_cmd:
        - 'openssl verify /etc/haproxy/ssl.d/{{ cert }}.crt'
  file.managed:
    - name: /etc/haproxy/ssl.d/{{ cert }}.crt
    - user: {{ "root" }}
    - group: {{ "root" }}
{%- endfor %}

haproxy_service:
  {%- if haproxy.service_enable is sameas true %}
  service.running:
    - name: {{ haproxy.service }}
    - enable: {{ haproxy.service_enable }}
    - reload: True
    - require:
      - pkg: haproxy_pkgs
  {%- else %}
  service.dead:
    - name: {{ haproxy.service }}
    - enable: False
  {%- endif %}

{%- if haproxy.service_enable is sameas true %}
haproxy_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 haproxy | grep Active | cut -d ':' -f 2,3,4 | xargs echo haproxy"
{%- endif %}

{%- endif %}
