# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Consul meta state

{%- from slspath ~ "/map.jinja" import consul with context %}

{%- if consul.enable is sameas true %}

consul_pkg:
  pkg.installed:
    - name: {{ consul.pkg }}
    - version: {{ consul.version }}
    - failhard: True
    - listen_in:
      - service: consul_server

{%- if consul.server is defined %}
consul_server_systemd:
  file.managed:
    - name: /etc/systemd/system/consul.service
    - source: {{ consul.systemd }}
    - template: jinja
    - mode: 755
    - context:
        kind: server
    - listen_in:
      - service: consul_server

consul_server_cnf:
  file.serialize:
    - name: {{ consul.server_file_path }}
    - formatter: json
    - dataset: {{ consul.server }}
    - user: {{ consul.user }}
    - group: {{ consul.group }}
    - mode: 0640
    - listen_in:
      - service: consul_server

consul_server:
  {%- if consul.service_enable is sameas true %}
  service.running:
    - name: {{ consul.service }}
    - enable: {{ consul.service_enable }}
    - reload: False
    - require:
      - pkg: consul_pkg
  {%- else %}
  service.dead:
    - name: {{ consul.service }}
    - enable: False
  {%- endif %}

consul_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 consul | grep Active | cut -d ':' -f 2,3,4 | xargs echo consul"
{%- endif %}

{%- if consul.agent is defined %}
{%- for dir in consul.directories %}
consul_dir_{{ dir }}:
  file.directory:
    - name: {{ dir }}
    - user: {{ consul.user }}
    - group: {{ consul.group }}
    - mode: 0750
{%- endfor %}

consul_agent_systemd:
  file.managed:
    - name: /etc/systemd/system/consul-agent.service
    - source: {{ consul.systemd }}
    - template: jinja
    - mode: 755
    - context:
        kind: agent
    - listen_in:
      - service: consul_agent

consul_agent_cnf:
  file.serialize:
    - name: {{ consul.agent_file_path }}
    - formatter: json
    - dataset: {{ consul.agent }}
    - user: {{ consul.user }}
    - group: {{ consul.group }}
    - mode: 0640
    - listen_in:
      - service: consul_agent

consul_agent:
  {%- if consul.service_enable is sameas true %}
  service.running:
    - name: consul-agent
    - enable: {{ consul.service_enable }}
    - reload: False
    - require:
      - pkg: consul_pkg
  {%- else %}
  service.dead:
    - name: {{ consul.agent }}
    - enable: False
  {%- endif %}

consul_agent_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 consul-agent | grep Active | cut -d ':' -f 2,3,4 | xargs echo consul-agent"
{%- endif %}

{%- endif %}
