# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Docker meta state

{%- from slspath ~ "/map.jinja" import docker with context %}

{% if docker.enable is sameas true %}

docker_pkgs:
  file.managed:
    - name: /usr/sbin/policy-rc.d
    - content: |
        #!/bin/bash
        exit 101
    - mode: 755
    - unless: test -f {{ docker.systemd_lib }}/{{ docker.service }}.service
  pkg.installed:
    - pkgs: {{ docker.pkgs | json }}
    - failhard: True

docker_daemon:
 file.managed:
    - name: {{ docker.daemon.path }}
    - source: {{ docker.daemon.src }}
    - template: jinja
    - makedirs: true
    - mode: 644
    - listen_in:
      - service: {{ docker.service }}

docker_directory:
 file.directory:
    - name: {{ docker.root_data }}
    - makedirs: true
    - mode: 750

docker_systemd:
  file.managed:
    - name: {{ docker.systemd_lib }}/{{ docker.service }}.service
    - source: {{ docker.systemd }}
    - template: jinja
    - mode: 644
    - listen_in:
      - service: {{ docker.service }}

docker_service:
  file.managed:
    - name: /usr/sbin/policy-rc.d
    - mode: 644
  {% if docker.service_enable is sameas true %}
  service.running:
    - name: {{ docker.service }}
    - enable: {{ docker.service_enable }}
    - reload: True
  {% else %}
  service.dead:
    - name: {{ docker.service }}
    - enable: False
  {% endif %}

{% if docker.registry.enable is sameas true %}
docker_registry_pkgs:
  pkg.installed:
    - name: "docker-registry"
    - failhard: True

docker_registry:
  file.managed:
    - name: {{ docker.registry.path }}
    - source: {{ docker.registry.src }}
    - template: jinja
    - makedirs: true
    - user: docker-registry
    - group: docker-registry
    - mode: 600
    - listen_in:
      - service: docker-registry

docker_service_registry:
  service.running:
    - name: docker-registry
    - enable: True
    - reload: False

docker_banner_registry:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 docker-registry | grep Active | cut -d ':' -f 2,3,4 | xargs echo registry"
{% endif %}

docker_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 docker | grep Active | cut -d ':' -f 2,3,4 | xargs echo docker"

{% endif %}
