# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Etcd state

{%- from slspath ~ "/map.jinja" import etcd with context %}

{%- if etcd.enable is sameas true %}

etcd_user:
  user.present:
    - name: {{ etcd.user }}
    - system: True
    - shell: /bin/bash
    - usergroup: True
    - createhome: True
    - home: {{ etcd.home }}

{% do etcd.directories.append(etcd.cert_dir) %}
{% for dir in etcd.directories %}
etcd-directory-{{ dir }}:
  file.directory:
    - name: {{ dir }}
    - user: {{ etcd.user }}
    - group: {{ etcd.group }}
    - dir_mode: 744
    - file_mode: 644
    - makedirs: True
{% endfor %}

etcd_install:
  cmd.run:
    - name: 'rm {{ etcd.home }}/v*'
    - unless: test -f {{ etcd.home }}/v{{ etcd.release }}
    - onlyif: test -f {{ etcd.home }}/etcd
  archive.extracted:
    - name: /tmp
    - source: {{ etcd.mirror }}/v{{ etcd.release }}/etcd-v{{ etcd.release }}-linux-amd64.tar.gz
    - user: {{ etcd.user }}
    - group: {{ etcd.group }}
    - skip_verify: true
    - keep_source: false
    - makedirs: true
    - enforce_toplevel: false
    - trim_output: true
    - unless: test -f {{ etcd.home }}/v{{ etcd.release }}
  rsync.synchronized:
    - name: {{ etcd.home }}
    - source: /tmp/etcd-v{{ etcd.release }}-linux-amd64/
    - force: true
    - unless: test -f {{ etcd.home }}/v{{ etcd.release }}
  file.touch:
    - name: {{ etcd.home }}/v{{ etcd.release }}
    - unless: test -f {{ etcd.home }}/v{{ etcd.release }}
    - listen_in:
      - service: etcd_service

etcd_systemd:
  file.managed:
    - name: {{ etcd.systemd_dir }}/{{ etcd.service }}.service
    - source: {{ etcd.systemd }}
    - template: jinja
    - listen_in:
      - service: etcd_service

etcd_cnf:
  file.managed:
    - name: /etc/default/etcd
    - source: {{ etcd.conf_src }}
    - template: jinja
    - listen_in:
      - service: etcd_service

etcd_symlink:
  file.symlink:
    - name: /usr/local/bin/etcdctl
    - target: {{ etcd.home }}/etcdctl

{%- for cert, data in etcd.get("certificates", {}) | dictsort %}
etcd_privkey_{{ cert }}:
  x509.private_key_managed:
    - name: {{ etcd.cert_dir }}/{{ cert }}.key
    - bits: {{ data.get('bits', 4096) }}
  file.managed:
    - name: {{ etcd.cert_dir }}/{{ cert }}.key
    - user: {{ etcd.user }}
    - group: {{ etcd.group }}
    - mode: 600

etcd_pubkey_{{ cert }}:
  x509.certificate_managed:
    - name: {{ etcd.cert_dir }}/{{ cert }}.crt
    - ca_server: {{ data.ca_server }}
    - signing_policy: internal
    - public_key: {{ etcd.cert_dir }}/{{ cert }}.key
    - CN: {{ data.get('cn', grains["id"]) }}
    - append_certs:
        - /usr/local/share/ca-certificates/ca.crt
    - subjectAltName: '{{ data.get('san', ["RID:1.2.3.4"]) | join(",") }}'
    - days_valid: {{ data.get('validity', 30) }}
    - days_remaining: {{ data.get('remain', 10) }}
    - check_cmd:
        - 'openssl verify {{ etcd.cert_dir }}/{{ cert }}.crt'
  file.managed:
    - name: {{ etcd.cert_dir }}/{{ cert }}.crt
    - user: {{ etcd.user }}
    - group: {{ etcd.group }}
{%- endfor %}

etcd_service:
  {% if etcd.service_enable is sameas true %}
  service.running:
    - name: {{ etcd.service }}
    - enable: {{ etcd.service_enable }}
    - reload: False
  grains.present:
    - name: etcd
    - force: true
    - value: 'existing'
  {% else %}
  service.dead:
    - name: {{ etcd.service }}
    - enable: False
  {% endif %}

etcd_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 etcd | grep Active | cut -d ':' -f 2,3,4 | xargs echo etcd"

{%- endif %}
