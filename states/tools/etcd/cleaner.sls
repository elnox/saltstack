# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Etcd state

{% from "etcd/map.jinja" import etcd with context %}

#
# Etcd is enabled
#

#
# Defrag
#
etcd-defrag:
  file.absent:
    - name: {{ etcd.home }}/bin/{{ etcd.defrag.script }}

etcd-defrag-logrotate:
  file.absent:
    - name: /etc/logrotate.d/etcd_defrag

etcd-defrag-systemd:
  file.absent:
    - name: {{ etcd.systemd_lib }}/{{ etcd.defrag.service }}

etcd-timer-systemd:
  file.absent:
    - name: {{ etcd.systemd_lib }}/{{ etcd.defrag.timer }}

etcd-banner-defrag:
  file.line:
    - mode: delete
    - name: /root/.systemctl_status
    - match: "etcd-defrag.service"
    - onlyif: test -f /root/.systemctl_status
