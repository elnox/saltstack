# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Libvirt meta state

{%- from slspath ~ "/map.jinja" import libvirt with context %}

{%- if libvirt.enable is sameas true %}

{%- set core = [] %}
{%- if libvirt.get("version") %}
{%-   for pkg in libvirt.pkgs %}{% do core.append(pkg ~ "=" ~ libvirt.version) %}{% endfor %}
{%- else %}
{%-   set core = libvirt.pkgs %}
{%- endif %}

libvirt_pkgs:
  pkg.installed:
    - pkgs: {{ core | json }}
    - failhard: True

libvirt_deps:
  pkg.installed:
    - pkgs: {{ libvirt.deps | json }}
    - failhard: True

libvirt_cnf:
  file.managed:
    - name: {{ libvirt.config_path }}
    - source: {{ libvirt.config_src }}
    - user: {{ libvirt.user }}
    - group: {{ libvirt.group }}
    - mode: {{ libvirt.mode }}
    - template: jinja
    - listen_in:
      - service: libvirt_service

libvirt_daemon:
  file.managed:
    - name: {{ libvirt.daemon_path }}
    - source: {{ libvirt.daemon_src }}
    - user: {{ libvirt.user }}
    - group: {{ libvirt.group }}
    - mode: {{ libvirt.mode }}
    - template: jinja
    - listen_in:
      - service: libvirt_service

{%- for name, data in libvirt.get("pool", {}) | dictsort %}
libvirt_pool_{{ name }}:
  {%- if data.type == "dir" %}
  file.directory:
    - name: {{ data.target }}
    - owner: {{ libvirt.owner }}
    - group: {{ libvirt.owner }}
    - makedirs: True
    - recurse: [ 'user', 'group' ]
  {%- endif %}
  virt.pool_{{ data.state }}:
    - name: {{ name }}
    - ptype: {{ data.type }}
    - target: {{ data.target }}
    - permissions:
        - mode: 0755
        - owner: {{ libvirt.owner }}
        - group: {{ libvirt.owner }}
    - autostart: {{ data.autostart | default(false) }}
{%- endfor %}

{%- for name, data in libvirt.get("network", {}) | dictsort %}
libvirt_network_{{ name }}:
  virt.network_{{ "running" if data.run | default(false) else "defined" }}:
    - name: {{ name }}
    - bridge: {{ data.bridge }}
    - forward: {{ data.forward }}
    - autostart: {{ data.autostart | default(false) }}
{%- endfor %}

{%- for name, data in libvirt.get("volume", {}) | dictsort %}
libvirt_volume_{{ name }}:
  virt.volume_defined:
    - name: {{ name }}
    - pool: {{ data.pool }}
    - size: {{ data.size }}
    - type: block
{%- endfor %}

{%- for name, data in libvirt.get("guest", {}) | dictsort %}
libvirt_clone_guest_{{ name }}:
  cmd.run:
    - name: "virt-clone -o {{ data.image }} --name {{ name }} --auto-clone"
    - onlyif:
      - fun: file.file_exists
        path: /etc/libvirt/qemu/{{ data.image }}.xml
    - unless:
      - fun: file.file_exists
        path: /etc/libvirt/qemu/{{ name }}.xml

libvirt_sysprep_guest_{{ name }}:
  cmd.run:
    - name: "virt-sysprep --domain {{ name }} --enable=machine-id,customize --hostname {{ name }} --firstboot-command 'sed -i \"s/{{ data.image }}/{{ name }}/\" /etc/hosts; systemctl start salt-minion'"
    - onchanges:
      - cmd: libvirt_clone_guest_{{ name }}

libvirt_boot_guest_{{ name }}:
  cmd.run:
    - name: "virsh start {{ name }}"
    - onchanges:
      - cmd: libvirt_sysprep_guest_{{ name }}
{%- endfor %}

libvirt_service:
  {%- if libvirt.service_enable is sameas true %}
  service.running:
    - name: {{ libvirt.service }}
    - enable: {{ libvirt.service_enable }}
    - reload: True
    - require:
      - pkg: libvirt_pkgs
    {%- if libvirt.watch is sameas true %}
    - watch:
      - pkg: libvirt_pkgs
    {%- endif %}
  {%- else %}
  service.dead:
    - name: {{ libvirt.service }}
    - enable: False
  {%- endif %}

{%- if libvirt.service_enable is sameas true %}
libvirt_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 libvirtd | grep Active | cut -d ':' -f 2,3,4 | xargs echo libvirtd"
{%- endif %}

{%- endif %}
