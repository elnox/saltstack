# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Tools meta state

include:
  - {{ slspath }}.bind
  - {{ slspath }}.consul
  - {{ slspath }}.etcd
  - {{ slspath }}.haproxy
  - {{ slspath }}.keepalived
  - {{ slspath }}.libvirt
  - {{ slspath }}.docker
  - {{ slspath }}.gns3
  - {{ slspath }}.kafka
