# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Bind meta state

{% from slspath ~ "/map.jinja" import bind with context %}

{%- if bind.enable is sameas true %}

bind_pkgs:
  pkg.installed:
    - pkgs: {{ bind.pkgs | json }}
    - failhard: True

{%- for dir in bind.directories %}
bind_{{ dir }}_dir:
  file.directory:
    - name: {{ dir }}
    - user: {{ bind.user }}
    - group: {{ bind.group }}
    - mode: 775
    - makedirs: True
    - require:
      - pkg: bind_pkgs
{%- endfor %}

{%- if bind.clean_config is sameas true %}
{%-   for file in bind.clean_file %}
bind_rm_{{ file }}:
  file.absent:
    - name: {{ bind.config_dir }}/{{ file }}
    - onlyif:
      - fun: file.file_exists
        path: {{ bind.config_dir }}/{{ file }}
{%-   endfor %}
{%- endif %}

{%- if bind.move_db is sameas true %}
{%-   for file in bind.move_db_file %}
bind_move_{{ file }}:
  file.rename:
    - name: {{ bind.config_dir }}/zones/{{ file }}
    - source: {{ bind.config_dir }}/{{ file }}
    - force: true
    - onlyif:
      - fun: file.file_exists
        path: {{ bind.config_dir }}/{{ file }}
{%-   endfor %}
{%- endif %}

{%- if bind.files is defined %}
{%-   for file in bind.files %}
bind_add_{{ file }}:
  file.managed:
    - name: {{ file }}
    - user: {{ bind.user }}
    - group: {{ bind.group }}
    - unless:
      - fun: file.file_exists
        path: {{ file }}
    - listen_in:
      - service: bind_service
{%-   endfor %}
{%- endif %}

{%- if bind.static is defined %}
{%-   for file, data in bind.static | dictsort %}
bind_add_static_{{ file }}:
  file.managed:
    - name: {{ data.path }}
    - source: {{ bind.static_src }}/{{ file }}
    - user: {{ bind.user }}
    - group: {{ bind.group }}
    {%- if data.force is not sameas true %}
    - unless:
      - fun: file.file_exists
        path: {{ file }}
    {%- endif %}
    - listen_in:
      - service: bind_service
{%-   endfor %}
{%- endif %}

{%- set x = bind.get("local_zones") if bind.get("root_zones") else {} %}
{%- set y = bind.get('available_zones', {}) %}
{%- set available_zones = salt['slsutil.merge'](x,y) %}

{%- for view, view_data in bind.configured_views | dictsort %}
{%-   set dash_view = '_' + view if view else '' %}
{%-   set a = bind.get('default_zones') if view_data.get("root_zones", false) else {} %}
{%-   set b = view_data.get('configured_zones', {}) %}
{%    for zone, zone_data in salt['slsutil.merge'](a,b) | dictsort -%}
{%-     set zone_spec = available_zones.get(zone, {}) %}
{%      if zone_spec["file"] is defined and zone_data['type'] == 'master' -%}
zones{{ dash_view }}_{{ zone }}:
  file.managed:
    - name: {{ bind.config_dir }}/{{ zone_spec["file"] }}
    - source: {{ bind.zone_src }}
    - template: jinja
    {% if zone_spec["records"] != {} %}
    - context:
      zone: {{ zone_spec }}
      include: False
    {% endif %}
    - user: {{ bind.user }}
    - group: {{ bind.group }}
    - mode: 644
    - listen_in:
      - service: bind_service
{%      endif %}
{%    endfor %}
{% endfor %}

bind_cnf:
  file.managed:
    - name: {{ bind.config_path }}
    - source: {{ bind.config_src }}
    - template: jinja
    - user: {{ bind.user }}
    - group: {{ bind.group }}
    - mode: 644
    - listen_in:
      - service: bind_service

bind_service:
  {%- if bind.service_enable is sameas true %}
  service.running:
    - name: {{ bind.service }}
    - enable: {{ bind.service_enable }}
    - reload: True
    - require:
      - pkg: bind_pkgs
  {%- else %}
  service.dead:
    - name: {{ bind.service }}
    - enable: False
  {%- endif %}

{%- endif %}
