# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Keepalived meta state

{%- from slspath ~ "/map.jinja" import keepalived with context %}

{%- if keepalived.enable is sameas true %}

keepalived_pkgs:
  pkg.installed:
    - pkgs: {{ keepalived.pkgs | json }}
    - failhard: True

keepalived_cnf:
  file.managed:
    - name: {{ keepalived.config_path }}
    - source: {{ keepalived.config_src }}
    - template: jinja
    - user: {{ keepalived.user }}
    - group: {{ keepalived.group }}
    - mode: {{ keepalived.mode }}
    - listen_in:
      - service: keepalived_service

{%- if salt['pillar.get']('keepalived:kernel', []) != [] %}
keepalived_module:
  file.managed:
    - name: {{ keepalived.module_path }}
    - source: {{ keepalived.module_src }}
    - template: jinja
{%- endif %}

keepalived_service:
  {% if keepalived.service_enable is sameas true %}
  service.running:
    - name: {{ keepalived.service }}
    - enable: {{ keepalived.service_enable }}
    - reload: True
    - require:
      - pkg: keepalived_pkgs
  {% else %}
  service.dead:
    - name: {{ keepalived.service }}
    - enable: False
  {% endif %}

{%- if keepalived.service_enable is sameas true %}
keepalived_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 keepalived | grep Active | cut -d ':' -f 2,3,4 | xargs echo keepalived"
{%- endif %}

{% endif %}
