# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Kafka meta state

{%- from slspath ~ "/map.jinja" import kafka with context %}

{%- if kafka.enable is sameas true %}

kafka_pkgs:
  pkg.installed:
    - pkgs: {{ kafka.pkgs }}
    - failhard: True
    - listen_in:
      - service: kafka_server

kafka_source_extract:
  archive.extracted:
    - name: /opt/
    - source: {{ kafka.mirror }}/{{ kafka.version }}/kafka_{{ kafka.scala }}-{{ kafka.version }}.tgz
    - skip_verify: true
    - keep_source: false
    - makedirs: true
  file.symlink:
    - name: /opt/kafka
    - target: /opt/kafka_{{ kafka.scala }}-{{ kafka.version }}

kafka_cnf:
  file.managed:
    - name: {{ kafka.server.path }}
    - source: {{ kafka.server.src }}
    - template: jinja
    - listen_in:
      - service: kafka_server

kafka_systemd:
  file.managed:
    - name: {{ kafka.systemd.path }}
    - source: {{ kafka.systemd.src }}
    - listen_in:
      - service: kafka_server

kafka_server:
  {% if kafka.service_enable is sameas true %}
  service.running:
    - name: {{ kafka.service }}
    - enable: {{ kafka.service_enable }}
    - reload: False
    - require:
      - pkg: kafka_pkgs
      - archive: kafka_source_extract
  {% else %}
  service.dead:
    - name: {{ kafka.service }}
    - enable: False
  {% endif %}

kafka_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 kafka | grep Active | cut -d ':' -f 2,3,4 | xargs echo kafka"

{% endif %}
