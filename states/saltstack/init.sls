# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Saltstack meta state

include:
  - {{ slspath }}.install
  - {{ slspath }}.master
  - {{ slspath }}.minion
  - {{ slspath }}.cloud
  - {{ slspath }}.patch
  - {{ slspath }}.service
  - {{ slspath }}.authority

{%- if not salt['grains.get']('bootstrap') %}
saltstack_bootstrap:
  grains.present:
    - name: bootstrap
    - force: true
    - value: true
{%- endif %}
