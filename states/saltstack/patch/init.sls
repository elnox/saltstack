# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Saltstack patch state

{%- from "saltstack/map.jinja" import saltstack with context %}

{%- for patch, set in saltstack.patchs | dictsort %}
saltstack_patch_{{ patch }}:
  file.patch:
    - name: {{ set.path }}
    - source: {{ saltstack.alt_path }}/{{ patch }}.diff
    - strip: {{ set.strip }}
    - reject_file: /tmp/{{ patch }}.rej
    {%- if set.revert is sameas true %}
    - options: -R
    - unless:
      - fun: match.grain
        tgt: 'patch:saltstack:{{ patch }}:False'
    {%- else %}
    - unless:
      - fun: match.grain
        tgt: 'patch:saltstack:{{ patch }}:True'
    {%- endif %}

saltstack_register_patch_{{ patch }}:
  grains.present:
    - name: patch:saltstack
    - force: true
    {%- if set.revert is sameas true %}
    - value:
      - {{ patch }}: false
    - unless:
      - fun: match.grain
        tgt: 'patch:saltstack:{{ patch }}:False'
    {%- else %}
    - value:
      - {{ patch }}: true
    - unless:
      - fun: match.grain
        tgt: 'patch:saltstack:{{ patch }}:True'
    {%- endif %}
    - require:
      - file: saltstack_patch_{{ patch }}
{%- endfor %}
