# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Saltstack minion state

{%- from "saltstack/map.jinja" import saltstack with context %}

{%- set minion = saltstack.minion %}

{%- if minion.enable is sameas true %}
{%- if minion.dependencies | length %}
saltstack_minion_dependencies:
  pkg.installed:
    - pkgs: {{ minion.dependencies }}
    - failhard: True
    - reload_modules: True
{%- endif %}

saltstack_minion_dir:
  file.directory:
    - name: {{ saltstack.config_path }}/minion.d
    - user: {{ saltstack.user }}
    - group: {{ saltstack.group }}

saltstack_minion_pki_dir:
  file.directory:
    - name: {{ saltstack.config_path }}/pki/minion
    - user: {{ saltstack.user }}
    - group: {{ saltstack.group }}
    - makedirs: True

{%- for file in minion.config %}
saltstack_minion_{{ file }}:
  file.managed:
    - name: {{ saltstack.config_path }}/minion.d/{{ file }}.conf
    - template: jinja
    - source: salt://saltstack/files/{{ file }}.jinja
    - context:
        scope: 'minion'
        standalone: False
    - require:
      - file: saltstack_minion_dir
{%- endfor %}

{%- if minion.remove_config %}
saltstack_clean_minion:
  file.absent:
    - name: {{ saltstack.config_path }}/minion
{%- endif %}

saltstack_minion_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 salt-minion | grep Active | cut -d ':' -f 2,3,4 | xargs echo salt-minion"
{%- endif %}
