# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Saltstack cloud state

{%- from "saltstack/map.jinja" import saltstack with context %}

{%- set cloud = saltstack.cloud %}

{%- if cloud.enable is sameas true %}
{%- if cloud.dependencies | length %}
saltstack_cloud_dependencies:
  pkg.installed:
    - pkgs: {{ cloud.dependencies }}
    - failhard: True
    - reload_modules: True
{%- endif %}

{%- for section in ["maps", "profiles", "providers", "deploy"] %}
saltstack_cloud_{{ section }}:
  file.directory:
    - name: {{ saltstack.config_path }}/cloud.{{ section }}.d
    - makedirs: True
{%- endfor %}

{% if cloud.bootstrap is defined %}
saltstack_cloud_bootstrap:
  file.managed:
    - name: {{ saltstack.config_path }}/cloud.deploy.d/{{ cloud.bootstrap }}
    - source: {{ saltstack.alt_path }}/{{ cloud.bootstrap }}
{% endif %}

{%- for provider in cloud.providers %}
{%-   set dataset = salt['pillar.get']('saltstack:cloud:providers:' + provider, {}) %}
saltstack_cloud_provider_{{ provider }}:
  file.managed:
    - name: {{ saltstack.config_path }}/cloud.providers.d/{{ provider }}.conf
    - source: {{ cloud.conf_provider }}
    - template: jinja
    - user: {{ saltstack.user }}
    - group: {{ saltstack.group }}
    - context:
      dataset: {{ dataset }}
    - require:
      - file: saltstack_cloud_providers
{%- endfor %}

saltcloud_providers_permissions:
  file.directory:
    - name: {{ saltstack.config_path }}/cloud.providers.d
    - user: {{ saltstack.user }}
    - group: {{ saltstack.group }}
    - file_mode: 600
    - dir_mode: 700
    - recurse: [ 'user', 'group', 'mode' ]
    - require:
      - file: saltstack_cloud_providers
{%- endif %}
