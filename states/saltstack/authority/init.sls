# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Saltstack master state

{%- from "saltstack/map.jinja" import saltstack with context %}

{%- set authority = saltstack.authority %}

{%- if authority.enable is sameas true %}
saltstack_x509_module:
  file.replace:
    - name: /usr/lib/python3/dist-packages/salt/states/x509.py
    - pattern: 'contents \+\= append_file_contents'
    - repl: |
        contents += str(append_file_contents.decode('utf-8'))
    - backup: false
    - ignore_if_missing: true
  pkg.installed:
    - pkgs: {{ authority.dependencies | json }}
    - failhard: true
    - reload_modules: True

saltstack_pki_dir:
  file.directory:
    - name: {{ authority.pki_dir }}
    - mode: 600
    - require:
      - file: saltstack_x509_module
      - pkg: saltstack_x509_module

{%- if saltstack.master.enable is sameas true %}
saltstack_ca_certs_dir:
  file.directory:
    - name: {{ authority.pki_dir }}/{{ authority.issued_dir }}
    - mode: 0600
    - require:
      - file: saltstack_pki_dir

saltstack_ca_signing_key:
  x509.private_key_managed:
    - name: {{authority.pki_dir }}/{{ authority.ca_privkey }}
    - bits: 4096
    - require:
      - file: saltstack_ca_certs_dir

saltstack_ca_root_cert:
  x509.certificate_managed:
    - name: {{ authority.pki_dir }}/{{ authority.ca_pubkey }}
    - signing_private_key: {{ authority.pki_dir }}/{{ authority.ca_privkey }}
    - CN: {{ grains['id'] }}
    - basicConstraints: "critical CA:true"
    - keyUsage: "critical cRLSign, keyCertSign"
    - subjectKeyIdentifier: hash
    - authorityKeyIdentifier: keyid,issuer:always
    - days_valid: 365
    - days_remaining: 0
    - require:
      - x509: saltstack_ca_signing_key

saltstack_ca_publish_root:
  module.run:
    - name: mine.send
    - m_name: x509.get_pem_entries
    - kwargs:
        glob_path: {{ authority.pki_dir }}/ca*
    - onchanges:
      - x509: saltstack_ca_root_cert

saltstack_ca_signing_policies:
  file.managed:
    - name: {{ authority.signing_cnf }}
    - source: {{ authority.signing_src }}
    - template: jinja
  service.running:
    - name: salt-minion
    - listen:
      - file: saltstack_ca_signing_policies
{%- endif %}

saltstack_internal_ca:
  file.directory:
    - name: {{ authority.ca_dir }}
  x509.pem_managed:
    - name: {{ authority.ca_dir }}/ca.crt
    - text: {{ salt['mine.get'](authority.trust, 'x509.get_pem_entries')[authority.trust][authority.pki_dir ~ '/' ~ authority.ca_pubkey] | replace('\n', '') }}
  cmd.run:
    - name: 'update-ca-certificates --fresh'
    - onchanges:
      - x509: saltstack_internal_ca

{%- for cert, data in saltstack.certificates | dictsort %}
{%-   if not data.get('absent', False) %}
saltstack_key_{{ cert }}:
  x509.private_key_managed:
    - name: {{ authority.pki_dir }}/{{ cert }}.key
    - bits: {{ data.get('bits', 4096) }}

saltstack_cert_{{ cert }}:
  x509.certificate_managed:
    - name: {{ authority.pki_dir }}/{{ cert }}.pem
    - ca_server: {{ authority.trust }}
    - signing_policy: internal
    - public_key: {{ authority.pki_dir }}/{{ cert }}.key
    - CN: {{ data.get('cn', grains["id"]) }}
    - append_certs:
        - {{ authority.ca_dir }}/{{ authority.ca_pubkey }}
    - subjectAltName: '{{ data.get('san', ["RID:1.2.3.4"]) | join(",") }}'
    - days_valid: {{ data.get('validity', 30) }}
    - days_remaining: {{ data.get('remain', 10) }}
    - check_cmd:
        - 'openssl verify {{ authority.pki_dir }}/{{ cert }}.pem'
  grains.list_present:
    - name: certificates
    - value: {{ authority.pki_dir }}/{{ cert }}.pem
    - onchanges:
      - x509: saltstack_cert_{{ cert }}
  event.send:
    - name: certificate/{{ grains['id'] }}/update/{{ cert }}
    - onchanges:
      - x509: saltstack_cert_{{ cert }}
{%-   else %}
saltstack_key_{{ cert }}:
  file.absent:
    - name: {{ authority.pki_dir }}/{{ cert }}.key

saltstack_cert_{{ cert }}:
  file.absent:
    - name: {{ authority.pki_dir }}/{{ cert }}.pem
  grains.list_absent:
    - name: certificates
    - value: {{ authority.pki_dir }}/{{ cert }}.pem
    - onchanges:
      - file: saltstack_cert_{{ cert }}
  event.send:
    - name: certificate/{{ grains['id'] }}/update/{{ cert }}
    - onchanges:
      - file: saltstack_cert_{{ cert }}
{%-   endif %}
{%- endfor %}
{%- endif %}
