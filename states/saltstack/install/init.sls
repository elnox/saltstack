# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Saltstack master state

{%- from "saltstack/map.jinja" import saltstack with context %}

{%- set pkgs = [] %}
{%- for item in [ "master", "minion", "cloud" ] %}
{%-   set comp = saltstack.get(item) %}
{%-   if comp.enable %}
{%-     if comp.get("version", None) %}
{%-       set pkg = "salt-" ~ item ~ "=" ~ comp.version %}
{%-     else %}
{%-       set pkg = "salt-" ~ item %}
{%-     endif %}
{%-     do pkgs.append(pkg) %}
{%-   endif %}
{%- endfor %}

{% if saltstack.minion.get("upgrade", false) or saltstack.master.get("upgrade", false) %}
saltstack_rc_policy_start:
  file.managed:
    - name: /usr/sbin/policy-rc.d
    - contents: |
        #!/bin/bash
        exit 101
    - mode: 755
    - replace: False
{% endif %}

saltstack_pkgs:
  pkg.installed:
    - pkgs: {{ pkgs }}
    - failhard: True
    {% if saltstack.minion.get("upgrade", false) or saltstack.master.get("upgrade", false) %}
    - order: last
    {% endif %}

saltstack_rc_policy_end:
  file.managed:
    - name: /usr/sbin/policy-rc.d
    - mode: 644

{% if saltstack.minion.get("upgrade", false) or saltstack.master.get("upgrade", false) %}
saltstack_upgrade_restart:
  cmd.run:
    - name: 'salt-call service.restart salt-minion'
    - bg: True
    - onchanges:
      - pkg: saltstack_pkgs
{% endif %}
