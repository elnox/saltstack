# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Saltstack master state

{%- from "saltstack/map.jinja" import saltstack with context %}

{%- set master = saltstack.master %}

{%- if master.enable is sameas true %}

{%- if master.dependencies | length %}
saltstack_master_dependencies:
  pkg.installed:
    - pkgs: {{ master.dependencies }}
    - failhard: True
    - reload_modules: True
{%- endif %}

saltstack_master_dir:
  file.directory:
    - name: {{ saltstack.config_path }}/master.d
    - user: {{ saltstack.user }}
    - group: {{ saltstack.group }}

saltstack_master_pki_dir:
  file.directory:
    - name: {{ saltstack.config_path }}/pki/master
    - user: {{ saltstack.user }}
    - group: {{ saltstack.group }}
    - makedirs: True

saltstack_master_autosign_dir:
  file.directory:
    - name: {{ saltstack.autosign_path }}
    - user: {{ saltstack.user }}
    - group: {{ saltstack.group }}
    - makedirs: True

{%- for file in master.config %}
saltstack_master_{{ file }}:
  file.managed:
    - name: {{ saltstack.config_path }}/master.d/{{ file }}.conf
    - template: jinja
    - source: salt://saltstack/files/{{ file }}.jinja
    - context:
      scope: 'master'
    - require:
      - file: saltstack_master_dir
{%- endfor %}

{%- if master.remove_config %}
saltstack_clean_master:
  file.absent:
    - name: {{ saltstack.config_path }}/master
{%- endif %}

{%- if master.multi_master %}
saltstack_multi_master:
  file.recurse:
    - name: {{ saltstack.config_path }}/pki/master
    - source: {{ saltstack.alt_path }}/keys
    - user: root
    - group: root
    - file_mode: 600
    - require:
      - file: saltstack_master_pki_dir
{%- endif %}

saltstack_gpgagent_config:
  file.append:
    - name: /etc/salt/gpgkeys/gpg-agent.conf
    - text:
      - "auto-expand-secmem 0x30000"
    - makedirs: True

saltstack_master_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 salt-master | grep Active | cut -d ':' -f 2,3,4 | xargs echo salt-master"
{%- endif %}
