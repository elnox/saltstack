# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Saltstack service state

{%- from "saltstack/map.jinja" import saltstack with context %}

{%- set service = saltstack.service %}

saltstack_master_service:
  {%- if saltstack.master.enable is sameas true %}
  service.running:
    - name: {{ service.master }}
    - enable: true
    - require:
      - sls: saltstack.master
    {%- if service.watch is sameas true %}
    - watch:
      - sls: saltstack.master
    {%- endif %}
  {%- else %}
  service.dead:
    - name: {{ service.master }}
    - enable: False
  {%- endif %}

saltstack_minion_service:
  {%- if saltstack.minion.enable is sameas true %}
  service.running:
    - name: {{ service.minion }}
    - enable: true
    - require:
      - sls: saltstack.minion
    {%- if service.watch is sameas true %}
    - watch:
      - sls: saltstack.minion
    {%- endif %}
  {%- else %}
  service.dead:
    - name: {{ service.minion }}
    - enable: False
  {%- endif %}
