# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Certificate expired renewer
{%- set minion_id = data['id'] %}
{%  if data['certificates'] | length -%}
trigger_renew_certificate:
  local.state.apply:
    - tgt: {{ minion_id }}
    - args:
      - mods:
        - saltstack.authority
      - concurrent: True
{% endif -%}
