# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Reload minion configuration
{%- set minion_id = data['id'] %}
trigger_salt_beacon:
  local.state.apply:
    - tgt: {{ minion_id }}
    - args:
      - mods:
        - orch.beacon
      - queue: True
