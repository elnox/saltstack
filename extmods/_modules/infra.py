import logging
import yaml

from salt.exceptions import CheckError

try:
    import ipaddress
    HAS_IPADDRESS = True
except ImportError:
    HAS_IPADDRESS = False

log = logging.getLogger(__name__)

__virtual_name__ = 'infra'

def __virtual__():
    '''
    Infra saltstack utils
    '''

    if HAS_IPADDRESS:
        return __virtual_name__
    else:
        return False, 'The infra module cannot be loaded: ipaddress package unavailable.'

def _check_object_keys(obj, mandatory_keys):
    for key in mandatory_keys:
        if key not in obj:
            err = ('Mandatory key \'{0}\' not found in submitted object \'{1}\'.').format(key, obj)
            raise CheckError(err)

    return True

def _get_range(cidr=None, wanted=None):
    if wanted == None:
        wanted = -1

    ip_range = []
    for ip in ipaddress.IPv4Network(cidr):
        split = str(ip).split(".")

        if int(split[-1]) != 0:
            ip_range.append(str(ip))

    return ip_range[:wanted]

def get_resolver(infra=None, domain="domain.tld"):
    log.debug(infra)

    fqdn    = __grains__["fqdn"]
    netmap  = {}

    resolvers = get_dnsips(infra)

    keys = [ 'information', 'features', 'interfaces', 'guests' ]
    for server in infra:

        _check_object_keys(infra[server], keys)

        servername  = server + "." + domain
        feat        = infra[server]['features']
        info        = infra[server]['information']
        interfaces  = infra[server]['interfaces']
        resolv  = []

        netmap[servername] = {}
        for ifname, iface in interfaces.items():
            ip = None
            if 'fixed' in iface:
                ip = iface['fixed']
            else:
                if 'gateway' in iface and not ip:
                    ip_pub  = _get_range(info['pubnet'], 1)
                    ip      = ip_pub[0]

            if ip in resolvers:
                resolv = [ ip ]
            else:
                resolv = resolvers

            if len(resolv):
                netmap[servername] = resolv
                break

        guests = infra[server]['guests']
        if 'hosts' in guests:
            for guest in guests['hosts']:
                guestname = guest + "." + feat['hypervisor'] + "." + domain
                netmap[guestname] = resolv

    return netmap[fqdn]

def get_network(infra=None, domain="domain.tld", netmask="/22"):
    log.debug(infra)

    fqdn    = __grains__["fqdn"]
    netmap  = {}

    resolvers = get_dnsips(infra)

    keys = [ 'information', 'features', 'interfaces', 'guests' ]
    for server in infra:

        _check_object_keys(infra[server], keys)

        servername  = server + "." + domain
        feat        = infra[server]['features']
        info        = infra[server]['information']
        interfaces  = infra[server]['interfaces']

        gateway = None
        resolv  = []

        netmap[servername] = {}
        for ifname, iface in interfaces.items():

            ip = None
            if 'fixed' in iface:
                ip = iface['fixed']
                #del iface['fixed']
            else:
                if 'gateway' in iface and not ip:
                    ip_pub  = _get_range(info['pubnet'], 1)
                    ip      = ip_pub[0]

                else:
                    ip_priv = _get_range(info['privnet'], 1)
                    ip      = ip_priv[0]

            iface['ip'] = ip + netmask

            if 'gateway' in iface and gateway == None:
                iface['dns-search'] = domain

                if ip in resolvers:
                    iface['resolv'] = [ ip ]
                else:
                    iface['resolv'] = resolvers

                gateway = iface["gateway"]
                resolv = iface['resolv']

            if 'bridge' in iface:
                bridged = iface['bridge']
                netmap[servername][bridged] = {
                    "manual": True,
                }

            netmap[servername][ifname] = iface

        guests = infra[server]['guests']

        if 'hosts' in guests:
            count=0
            for guest in guests['hosts']:
                guestname = guest + "." + feat['hypervisor'] + "." + domain

                if 'ifaces' in guests:
                    ifaces = {}
                    for ifname in guests['ifaces']:
                        ips = _get_range(guests['ifaces'][ifname])
                        iface_ip  = ips[count] + netmask
                        ifaces[ifname] = {
                            "ip":   iface_ip,
                            "auto": True,
                        }

                        if gateway != None:
                            gw      = ipaddress.ip_address(gateway)
                            cidr    = ipaddress.ip_network(iface_ip, strict=False)

                            if gw in cidr:
                                ifaces[ifname]['gateway']       = gateway
                                ifaces[ifname]['dns-search']    = domain
                                ifaces[ifname]['resolv']        = resolv

                    netmap[guestname] = ifaces

                count += 1

    return netmap[fqdn]

def _dns_ips(ifaces, network):
    ip = []

    for iface in ifaces:
        if "fixed" in ifaces[iface]:
            addr = ipaddress.ip_address(ifaces[iface]['fixed'])
            cidr = ipaddress.ip_network(network)
            if addr in cidr:
                ip.append(ifaces[iface]['fixed'])

    if not ip:
        ip = _get_range(network, 1)

    return ip

def _is_master(infra=None):
    for server in infra:
        feat = infra[server]['features']
        if "nameserver" in feat and  feat["nameserver"] == "master":
            if __grains__["host"] == server:
                return True

    return False

def get_dnsips(infra=None, kind=None):
    ips = []

    keys = [ 'features', 'interfaces' ]
    for server in infra:
        _check_object_keys(infra[server], keys)

        feat        = infra[server]['features']
        info        = infra[server]['information']
        ifaces      = infra[server]['interfaces']

        if "nameserver" in feat and kind == None:
            ip = _dns_ips(ifaces, info["pubnet"])
            ips.append(ip[0])

        elif "nameserver" in feat and feat["nameserver"] == kind:
            ip = _dns_ips(ifaces, info["pubnet"])
            ips.append(ip[0])

    return ips

def get_dnsview(infra=None, domain="domain.tld"):
    log.debug(infra)

    master  = False
    kind    = "slave"
    slaves  = []
    if _is_master(infra):
        master  = True
        kind    = "master"
        slaves  = get_dnsips(infra)
        slaves.append("127.0.0.0/8")

    dnsmap = {}

    keys = [ 'information', 'features' ]
    for server in infra:

        _check_object_keys(infra[server], keys)

        feat    = infra[server]['features']
        info    = infra[server]['information']
        ip      = _get_range(info['pubnet'], 1)

        zonename = feat["hypervisor"] + "." + domain
        if feat["hypervisor"] == "ha00":
            zonename = domain
        zone = {
            "zonename":         zonename,
            "comment":          "zone " + zonename,
            "zone-statistics":  True,
            "type":             kind,
            "notify":           master,
        }

        if master:
            zone["allow-transfer"] = slaves
        else:
            zone["masterfile-format"] = "text"

        dnsmap[zonename] = zone

        reverse = ipaddress.ip_address(ip[0]).reverse_pointer
        revzone = {
            "comment":  feat["hypervisor"] + " reverse " + ip[0] + "/24",
            "type":     kind,
            "notify":   master,
        }

        if master:
            revzone["allow-transfer"] = slaves
        else:
            revzone["masterfile-format"] = "text"

        revname = reverse.split(".", 1)
        dnsmap[revname[1]] = revzone

    return dnsmap

def get_dnszone(infra=None, soa={}, domain="drakonix.loc"):
    log.debug(infra)

    master  = False
    kind    = "slave"
    slaves  = []
    if _is_master(infra):
        master  = True
        kind    = "master"
        slaves  = get_dnsips(infra, "slave")
        slaves.append("127.0.0.0/8")

    dnsmap = {}

    keys = [ "information", "features", "guests" ]
    for server in infra:

        _check_object_keys(infra[server], keys)

        feat    = infra[server]['features']
        info    = infra[server]['information']
        ip      = _get_range(info['pubnet'], 1)

        if feat["hypervisor"] == "ha00":
            zonename    = domain
            dbfile      = "zones/db." + zonename
        else:
            zonename    = feat["hypervisor"] + "." + domain
            dbfile      = "zones/" + zonename

        zone = {
            "domain":   zonename,
            "soa":      soa,
            "file":     dbfile,
            "masters":  [ "192.168.0.4" ],
        }

        records = {
            "NS": { '@': [ "ns1." + domain + "." ] },
            "MX": { '@': [ "10 mx1." + domain + "." ] },
        }

        if feat["hypervisor"] == "ha00":
            arecords = {
                "00 - authority and mx": {
                    "mx1": "192.168.0.4",
                    "ns1": "192.168.0.4",
                    "@": "192.168.0.100",
                    "*": "192.168.0.100",
                },
                "00 - baremetal - cidr /28": {
                    "gw": "192.168.0.1",
                    "kali": "192.168.0.4",
                },
                "01 - guests - cidr /28": {},
            }
        else:
            arecords = {
                "00 - baremetal - cidr /28": { "@": ip[0] },
                "01 - guests - cidr /28": {},
            }

        crecords = {
            'blackbox': "ha01",
            'whitebox': "ha02",
            'greenbox': "ha03",
            'salt': "kali",
        }

        guests = infra[server]['guests']

        if 'hosts' in guests:
            cidr = info["pubnet"]
            ips = []
            if 'ifaces' in guests:
                for iface in guests['ifaces']:
                    net = guests['ifaces'][iface]
                    if ipaddress.IPv4Network(cidr).overlaps(ipaddress.IPv4Network(net)):
                        ips = _get_range(net)

            if ips:
                count=0
                for guest in guests['hosts']:
                    guest_ip = ips[count]
                    count += 1
                    arecords["01 - guests - cidr /28"][guest] = guest_ip

        if feat["hypervisor"] == "ha00":
            records["CNAME"] = crecords

        records["A"] = arecords
        zone["records"] = records
        dnsmap[zonename] = zone

        if feat["hypervisor"] == "ha00":
            zonename = "gw." + domain

        reverse = ipaddress.ip_address(ip[0]).reverse_pointer
        revname = reverse.split(".", 1)
        revzone = {
            "file":     "zones/db.rev." + revname[1],
            "soa":      soa,
            "masters":  [ "192.168.0.4" ],
        }

        records = {
            "NS": { '@': [ "ns1." + domain + "." ] },
        }

        precords = {
            "00 - baremetal - cidr /28": { "1": zonename + "." },
            "01 - guests - cidr /28": {},
        }

        if feat["hypervisor"] == "ha00":
            precords["00 - baremetal - cidr /28"]["4"] = "kali." + domain + "."

        if 'hosts' in guests:
            cidr = info["pubnet"]
            ips = []
            if 'ifaces' in guests:
                for iface in guests['ifaces']:
                    net = guests['ifaces'][iface]
                    if ipaddress.IPv4Network(cidr).overlaps(ipaddress.IPv4Network(net)):
                        ips = _get_range(net)

            if ips:
                count=0
                for guest in guests['hosts']:
                    guest_ip = ips[count]
                    count += 1
                    octet = guest_ip.split('.')[-1]
                    precords["01 - guests - cidr /28"][octet] = guest + "." + zonename + "."

        records["PTR"] = precords
        revzone["records"] = records

        dnsmap[revname[1]] = revzone

    return dnsmap

def get(infra=None):
    log.debug(infra)

    return infra
