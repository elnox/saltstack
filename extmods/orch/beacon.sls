# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Control state

beacon_reload:
  file.managed:
    - name: /etc/salt/minion.d/beacon.conf
    - template: jinja
    - source: salt://saltstack/files/beacon.jinja
    - context:
        scope: 'minion'
        standalone: False
  module.run:
    - name: saltutil.refresh_beacons
    - onchanges:
      - file: beacon_reload
