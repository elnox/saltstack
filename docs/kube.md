# Kubernetes

## Infrastructure

```
                     +-------------+  +-------------+  +-------------+           x
                     |  etcd.ha01  |<>|  etcd.ha03  |<>|  etcd.ha02  |           |
                     +------+------+  +------+------+  +------+------+           |
                            |                |                |                  |
                            +----------------+----------------+                  |
                                             |                                   |
                            +----------------+----------------+                  |
                            |                |                |                  |
                     +------+------+  +------+------+  +------+------+           |
                     | master.ha01 |<>| master.ha03 |<>| master.ha02 |           | net b: 172.16.0.0/16
                     +------+------+  +------+------+  +------+------+           |
                            |                |                |                  |
                            +----------------+----------------+                  |
+--------------+                             |                                   |
| gateway.ha03 |<--------------------------->+                                   |
+------+-------+                             |                                   |
       |                       +-------------+-------------+                     |
       |                       |                           |                     |
+------+------+         +------+------+             +------+------+              x
|    laptop   |         |  node.ha01  |             |  node.ha02  |
+-------------+         +------+------+             +------+------+              x
                               |                           |                     | net d: 172.16.160.0/19
                     +-----------------------------------------------+           x
                     |                  containers                   |
                     +-----------------------+-----------------------+
                                             X
                     +-----------------------+-----------------------+           x
                     |                   services                    |           | net e: 192.168.128.0/19
                     +-----------------------------------------------+           x
```

## Bootstrap

Set admin token
```
TOKEN=$(kubectl -n kube-system describe secret default| awk '$1=="token:"{print $2}')
kubectl config set-credentials kubernetes-admin --token="${TOKEN}"
```

Remove uneschedulable taint
```
k taint node node.ha0x.drakonix.loc unschedulable=true:NoSchedule-
```

Apply helloworld deployment and service
```
k apply -f ./files/k8s/hello/deployment.yml
k apply -f ./files/k8s/hello/service.yml
```

## Cheat sheet

List resources
```
k get namespaces
k get nodes -A -o wide
k get deployments -A -o wide
k get pods -A -o wide
k get replicaset -A -o wide
```

Describe resources
```
k describe namespaces default
k describe nodes node.ha01.drakonix.loc
k describe deploy helloworld
k describe pods helloworld-797cf87dd8-gkx74
k describe replicasets helloworld-797cf87dd8
```

Edit resources
```
k edit namespaces default
k edit nodes node.ha01.drakonix.loc
k edit deploy helloworld
k edit pods helloworld-797cf87dd8-gkx74
k edit replicasets helloworld-797cf87dd8
```

Connect to a pod
```
k exec -it helloworld-797cf87dd8-gkx74 -- /bin/bash
```
