# KVM virtual machine management

The libvirt states should already have deployed some resources, let's take a look at what should be already available:
```
virsh net-list --all
 Name       State      Autostart   Persistent
-----------------------------------------------
 bridge-0   active     yes         yes
 bridge-1   active     yes         yes
 default    inactive   no          yes

virsh pool-list --all
Name      State    Autostart
-------------------------------
 default   active   yes
 livecd    active   yes
 vg1       active   yes

virsh vol-list --pool vg1
 Name       Path
-------------------------------
 bullseye   /dev/vg1/bullseye
```

With all these resources available we will create a new debian 11 based virtual machine:
```
virt-install --connect qemu:///system \
    -n bullseye -r 4096 --vcpus=4 \
    --os-type=linux --os-variant=debian10 \
    --disk vol=vg1/bullseye \
    --disk vol=livecd/debian-11.0.0-amd64.iso,device=cdrom \
    --network network=bridge-0 \
    --network network=bridge-1 \
    --graphics spice --video qxl --channel spicevmc \
    --noautoconsole --hvm --boot hd,cdrom
```

Ensure to set the domain of the server (ha0x.domain.tld)
Finish the installation by connecting to the newly deployed server using virt-manager:
```
virt-manager -c 'qemu:///system'

apt update && apt upgrade -y && apt autoremove -y

mkdir /root/.ssh
echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOd0yidp3B3aCNJR83U6BvzaXUUaLSbfckpODJDmZXau root@kali.drakonix.net" > /root/.ssh/authorized_keys
echo "deb http://repo.saltstack.com/py3/debian/10/amd64/3003 buster main" > /etc/apt/sources.list.d/saltstack.list
apt install gnupg -y

wget -O - https://repo.saltstack.com/py3/debian/10/amd64/3003/SALTSTACK-GPG-KEY.pub | apt-key add -
echo -e '#!/bin/sh\nexit 101' > /usr/sbin/policy-rc.d
chmod a+x /usr/sbin/policy-rc.d
apt install htop salt-minion=3003.3+ds-1 salt-common=3003.3+ds-1 -y

echo "192.168.0.4     salt" >> /etc/hosts
chmod a-x /usr/sbin/policy-rc.d
systemctl disable salt-minion
rm /etc/apt/sources.list.d/saltstack.list
/sbin/usermod -aG sudo yoda
echo "startup_states: 'highstate'" > /etc/salt/minion
```

When finished we can shutdown the vm and ensure autostart is off
```
virsh shutdown bullseye && virsh autostart --disable bullseye
```

Now that we have built our bullseye template we will clean it up to prepare virtual machine cloning:
```
virt-sysprep --domain bullseye --enable=abrt-data,backup-files,bash-history,cron-spool,dhcp-client-state,dhcp-server-state,logfiles,mail-spool,net-hostname,net-hwaddr,package-manager-cache,rhn-systemid,ssh-hostkeys,tmp-files,udev-persistent-net,utmp,yum-uuid,customize --firstboot-command 'dpkg-reconfigure openssh-server'
```

Finally we are ready to clone a new virtual machine based on our template:
```
virt-clone -o bullseye --name test --auto-clone
virt-sysprep --domain test --enable=machine-id,customize --hostname test --firstboot-command 'sed -i "s/bullseye/test/" /etc/hosts; systemctl start salt-minion'
virsh start test
```
