# Docker

## Infrastructure

```
              +---------------------------------------------------------+           x
              |                        containers                       |           |
              +------+---------------------+---------------------+------+           | net c: 172.17.0.0/16
                     |                     |                     |                  |
              +------+------+       +------+------+       +------+------+           x
              | docker.ha01 |       | docker.ha03 |       | docker.ha02 |
              +------+------+       +------+------+       +------+------+           x
                     |                     |                     |                  |
                     +---------------------+---------------------+                  |
                                           |                                        | net a: 192.168.0.0/16
+-------------+                     +------+------+                                 |
|    laptop   | +-----------------> |   registry  |                                 |
+-------------+                     +-------------+                                 x
```

## Cheat sheet

Pull and push image to our private registry
```
docker pull debian:bullseye
docker tag debian:bullseye registry.drakonix.loc/debian
docker push registry.drakonix.loc/debian
docker image remove debian:bullseye
docker image remove registry.drakonix.loc/debian:latest
docker pull registry.drakonix.loc/debian
```

Delete image from our private registry
```
curl https://registry.drakonix.loc/v2/_catalog
curl https://registry.drakonix.loc/v2/debian/tags/list
curl -v --silent -H "Accept: application/vnd.docker.distribution.manifest.v2+json" -X GET https://registry.drakonix.loc/v2/debian/manifests/latest 2>&1 | grep Docker-Content-Digest | awk '{print ($3)}'
curl -v --silent -H "Accept: application/vnd.docker.distribution.manifest.v2+json" -X DELETE https://registry.drakonix.loc/v2/debian/manifests/sha256:826d8850098b954c3d67c2e45ea7a08fa681c128126ba01b35bd2689b1bb2e04
/usr/bin/docker-registry garbage-collect  /etc/docker/registry/config.yml
```
