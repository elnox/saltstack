#!/bin/sh -

#======================================================================================================================
# vim: softtabstop=4 shiftwidth=4 expandtab fenc=utf-8 spell spelllang=en cc=120
#======================================================================================================================
# Treat unset variables as an error
set -o nounset

# ! Don't change the ScriptName as it will break the deployment
__ScriptVersion="2020.08.16"
__ScriptName="bootstrap-salt.sh"
__ScriptFullName="$0"
__ScriptArgs="$*"

#======================================================================================================================
#  Environment variables taken into account.
#
#   * BS_COLORS:                If 0 disables colour support
#   * BS_ECHO_DEBUG:            If 1 enable debug echo which can also be set by -D
#   * BS_UPGRADE_SYS:           If 1 and an option, upgrade system. Default 0.
#   * BS_SALT_MASTER_ADDRESS:   The IP or DNS name of the salt-master the minion should connect to
#======================================================================================================================

BS_TRUE=1
BS_FALSE=0
BS_DISTRO="Debian"
BS_HTTP_VAL="https"

_REPO_URL="repo.saltstack.com"

__DEFAULT_SLEEP=3
__PACKAGES='wget apt-transport-https ca-certificates gnupg2 procps pciutils'
__REPO_ARCH="amd64"
__REPO_ARCH_DEB='deb'

# Defaults for install arguments
ITYPE="stable"

#---  FUNCTION  -------------------------------------------------------------------------------------------------------
#          NAME:  __detect_color_support
#   DESCRIPTION:  Try to detect color support.
#----------------------------------------------------------------------------------------------------------------------
_COLORS=${BS_COLORS:-$(tput colors 2>/dev/null || echo 0)}
# shellcheck disable=SC2181
if [ $? -eq 0 ] && [ "$_COLORS" -gt 2 ]; then
    RC='\033[1;31m'
    GC='\033[1;32m'
    BC='\033[1;34m'
    YC='\033[1;33m'
    EC='\033[0m'
else
    RC=""
    GC=""
    BC=""
    YC=""
    EC=""
fi

#----------------------------------------------------------------------------------------------------------------------
#  Printer functions
#----------------------------------------------------------------------------------------------------------------------
echoerror() {
    printf "${RC} * ERROR${EC}: %s\\n" "$@" 1>&2;
}

echoinfo() {
    printf "${GC} *  INFO${EC}: %s\\n" "$@";
}

echowarn() {
    printf "${YC} *  WARN${EC}: %s\\n" "$@";
}

echodebug() {
    if [ "$_ECHO_DEBUG" -eq $BS_TRUE ]; then
        printf "${BC} * DEBUG${EC}: %s\\n" "$@";
    fi
}

#----------------------------------------------------------------------------------------------------------------------
#  Checker functions
#----------------------------------------------------------------------------------------------------------------------
__check_command_exists() {
    command -v "$1" > /dev/null 2>&1
}

__check_config_dir() {
    CC_DIR_NAME="$1"
    CC_DIR_BASE=$(basename "${CC_DIR_NAME}")

    if [ ! -e "${CC_DIR_NAME}" ]; then
        echoerror "The configuration directory or archive $CC_DIR_NAME does not exist."
        echo "null"
        return
    fi

    case "$CC_DIR_NAME" in
        *.tgz|*.tar.gz)
            tar -zxf "${CC_DIR_NAME}" -C /tmp
            CC_DIR_BASE=$(basename "${CC_DIR_BASE}" ".tgz")
            CC_DIR_BASE=$(basename "${CC_DIR_BASE}" ".tar.gz")
            CC_DIR_NAME="/tmp/${CC_DIR_BASE}"
            ;;
        *.tbz|*.tar.bz2)
            tar -xjf "${CC_DIR_NAME}" -C /tmp
            CC_DIR_BASE=$(basename "${CC_DIR_BASE}" ".tbz")
            CC_DIR_BASE=$(basename "${CC_DIR_BASE}" ".tar.bz2")
            CC_DIR_NAME="/tmp/${CC_DIR_BASE}"
            ;;
        *.txz|*.tar.xz)
            tar -xJf "${CC_DIR_NAME}" -C /tmp
            CC_DIR_BASE=$(basename "${CC_DIR_BASE}" ".txz")
            CC_DIR_BASE=$(basename "${CC_DIR_BASE}" ".tar.xz")
            CC_DIR_NAME="/tmp/${CC_DIR_BASE}"
            ;;
    esac

    echo "${CC_DIR_NAME}"
}

#----------------------------------------------------------------------------------------------------------------------
#  Usage function
#----------------------------------------------------------------------------------------------------------------------
__usage() {
    cat << EOT

  Usage :  ${__ScriptName} [options] <install-type> [install-type-args]

  Installation types:
    - stable              Install latest stable release.
    - stable [branch]     Install latest version on a branch.
    - stable [version]    Install a specific version.

  Examples:
    - ${__ScriptName}
    - ${__ScriptName} stable
    - ${__ScriptName} stable 2017.7
    - ${__ScriptName} stable 2017.7.2

  Options:
    -h  Display this message
    -v  Display script version
    -D  Show debug output
    -c  Temporary configuration directory
    -s  Sleep time used when waiting for daemons to start, restart and when
        checking for the services running. Default: ${__DEFAULT_SLEEP}
    -U  If set, fully upgrade the system prior to bootstrapping Salt
    -A  Pass the salt-master DNS name or IP. This will be stored under
        \${BS_SALT_ETC_DIR}/minion.d/99-master-address.conf
    -i  Pass the salt-minion id. This will be stored under
        \${BS_SALT_ETC_DIR}/minion_id
    -x  Changes the Python version used to install Salt.

EOT
}

#----------------------------------------------------------------------------------------------------------------------
#  Handle command line arguments
#----------------------------------------------------------------------------------------------------------------------
_TEMP_CONFIG_DIR="null"
_SLEEP="${__DEFAULT_SLEEP}"
_ECHO_DEBUG=${BS_ECHO_DEBUG:-$BS_FALSE}
_SALT_ETC_DIR=${BS_SALT_ETC_DIR:-/etc/salt}
_PKI_DIR=${_SALT_ETC_DIR}/pki
_UPGRADE_SYS=${BS_UPGRADE_SYS:-$BS_FALSE}
_SALT_MASTER_ADDRESS=${BS_SALT_MASTER_ADDRESS:-null}
_SALT_MINION_ID="null"
_PY_EXE=""

while getopts ':hvnDc:g:Gyx:wk:s:MSNXCPFUKIA:i:Lp:dH:bflV:J:j:rR:aq' opt
do
  case "${opt}" in

    h )  __usage; exit 0                                ;;
    v )  echo "$0 -- Version $__ScriptVersion"; exit 0  ;;
    D )  _ECHO_DEBUG=$BS_TRUE                           ;;
    c )  _TEMP_CONFIG_DIR="$OPTARG"                     ;;
    s )  _SLEEP=$OPTARG                                 ;;
    U )  _UPGRADE_SYS=$BS_TRUE                          ;;
    A )  _SALT_MASTER_ADDRESS=$OPTARG                   ;;
    i )  _SALT_MINION_ID=$OPTARG                        ;;
    x )  _PY_EXE="$OPTARG"                              ;;

    \?)  echo
         echoerror "Option does not exist : $OPTARG"
         __usage && exit 1
         ;;

  esac
done
shift $((OPTIND-1))

# Define our logging file and pipe paths
LOGFILE="/tmp/$( echo "$__ScriptName" | sed s/.sh/.log/g )"
LOGPIPE="/tmp/$( echo "$__ScriptName" | sed s/.sh/.logpipe/g )"

# Ensure no residual pipe exists
rm "$LOGPIPE" 2>/dev/null

# Create our logging pipe
if ! (mknod "$LOGPIPE" p >/dev/null 2>&1); then
    echoerror "Failed to create the named pipe required to log" && exit 1
fi

# What ever is written to the logpipe gets written to the logfile
tee < "$LOGPIPE" "$LOGFILE" &

# Close STDOUT, reopen it directing it to the logpipe
exec 1>&-
exec 1>"$LOGPIPE"

# Close STDERR, reopen it directing it to the logpipe
exec 2>&-
exec 2>"$LOGPIPE"

#----------------------------------------------------------------------------------------------------------------------
#  Exit function
#----------------------------------------------------------------------------------------------------------------------
APT_ERR=$(mktemp /tmp/apt_error.XXXXXX)
__exit_cleanup() {
    EXIT_CODE=$?

    # Remove the logging pipe when the script exits
    if [ -p "$LOGPIPE" ]; then
        echodebug "Removing the logging pipe $LOGPIPE"
        rm -f "$LOGPIPE"
    fi

    # Remove the temporary apt error file when the script exits
    if [ -f "$APT_ERR" ]; then
        echodebug "Removing the temporary apt error file $APT_ERR"
        rm -f "$APT_ERR"
    fi

    # Kill tee when exiting, CentOS, at least requires this
    # shellcheck disable=SC2009
    TEE_PID=$(ps ax | grep tee | grep "$LOGFILE" | awk '{print $1}')

    [ "$TEE_PID" = "" ] && exit $EXIT_CODE

    echodebug "Killing logging pipe tee's with pid(s): $TEE_PID"

    # We need to trap errors since killing tee will cause a 127 errno
    # We also do this as late as possible so we don't "mis-catch" other errors
    __trap_errors() {
        echoinfo "Errors Trapped: $EXIT_CODE"
        # Exit with the "original" exit code, not the trapped code
        exit $EXIT_CODE
    }
    trap "__trap_errors" INT ABRT QUIT TERM

    # Now we're "good" to kill tee
    kill -s TERM "$TEE_PID"

    # In case the 127 errno is not triggered, exit with the "original" exit code
    exit $EXIT_CODE
}

#----------------------------------------------------------------------------------------------------------------------
#  Handle basic checkings
#----------------------------------------------------------------------------------------------------------------------
trap "__exit_cleanup" EXIT INT

echoinfo "Running version: ${__ScriptVersion}"
echoinfo "Command line: '${__ScriptFullName} ${__ScriptArgs}'"

# Force installation type to stable
if [ "$#" -gt 0 ];then
    shift
fi

# Check if version specified use latest if not
if [ "$#" -eq 0 ];then
    STABLE_REV="latest"
else
    STABLE_REV="$1"
    shift
fi

# Check for any unparsed arguments. Should be an error.
if [ "$#" -gt 0 ]; then
    __usage
    echo
    echoerror "Too many arguments." && exit 1
fi

# Root permissions are required to run this script
whoami='whoami'
if [ "$($whoami)" != "root" ]; then
    echoerror "Salt requires root privileges to install. Please re-run this script as root." && exit 1
fi

# Check if we're installing via a different Python executable and set major version variables
if [ -n "$_PY_EXE" ]; then
    _PY_PKG_VER=$(echo "$_PY_EXE" | sed -E "s/\\.//g")
    _PY_MAJOR_VERSION=$(echo "$_PY_PKG_VER" | cut -c 7)
    if [ "$_PY_MAJOR_VERSION" != 3 ] && [ "$_PY_MAJOR_VERSION" != 2 ]; then
        echoerror "Detected -x option, but Python major version is not 2 or 3."
        echoerror "The -x option must be passed as python2, python27, or python2.7 (or use the Python '3' versions of examples)."
        exit 1
    fi

    echoinfo "Detected -x option. Using $_PY_EXE to install Salt."
else
    _PY_PKG_VER=""
fi

# If the configuration directory or archive does not exist, error out
if [ "$_TEMP_CONFIG_DIR" != "null" ]; then
    _TEMP_CONFIG_DIR="$(__check_config_dir "$_TEMP_CONFIG_DIR")"
    [ "$_TEMP_CONFIG_DIR" = "null" ] && exit 1
fi

#----------------------------------------------------------------------------------------------------------------------
#  Utils functions
#----------------------------------------------------------------------------------------------------------------------
__fetch_url() {
    # shellcheck disable=SC2086
    curl -L -s -o "$1" "$2" >/dev/null 2>&1 || wget -q -O "$1" "$2" >/dev/null 2>&1
}

__parse_version_string() {
    VERSION_STRING="$1"
    PARSED_VERSION=$(
        echo "$VERSION_STRING" |
        sed -e 's/^/#/' \
            -e 's/^#[^0-9]*\([0-9][0-9]*\.[0-9][0-9]*\)\(\.[0-9][0-9]*\).*$/\1/' \
            -e 's/^#[^0-9]*\([0-9][0-9]*\.[0-9][0-9]*\).*$/\1/' \
            -e 's/^#[^0-9]*\([0-9][0-9]*\).*$/\1/' \
            -e 's/^#.*$//'
    )
    echo "$PARSED_VERSION"
}

#----------------------------------------------------------------------------------------------------------------------
#  Gathering Debian System Informations
#----------------------------------------------------------------------------------------------------------------------
if [ -f /proc/cpuinfo ]; then
    CPU_VENDOR_ID=$(awk '/vendor_id|Processor/ {sub(/-.*$/,"",$3); print $3; exit}' /proc/cpuinfo )
else
    CPU_VENDOR_ID=$( sysctl -n hw.model )
fi

# shellcheck disable=SC2034
CPU_VENDOR_ID_L=$( echo "$CPU_VENDOR_ID" | tr '[:upper:]' '[:lower:]' )
CPU_ARCH=$(uname -m 2>/dev/null || uname -p 2>/dev/null || echo "unknown")

OS_NAME=$(uname -s 2>/dev/null)
OS_NAME_L=$( echo "$OS_NAME" | tr '[:upper:]' '[:lower:]' )
OS_VERSION=$(uname -r)

if [ ${OS_NAME_L} != 'linux' ]; then
    echoerror "${OS_NAME} not supported." && exit 1
fi

DISTRO_VERSION=""

# Let's test if the lsb_release binary is available
rv=$(lsb_release >/dev/null 2>&1)

# shellcheck disable=SC2181
if [ $? -eq 0 ]; then
    rv=$(lsb_release -sr)
    [ "${rv}" != "" ] && DISTRO_VERSION=$(__parse_version_string "$rv")
elif [ -f /etc/lsb-release ]; then
    rv=$(grep DISTRIB_RELEASE /etc/lsb-release | sed -e 's/.*=//')
    [ "${rv}" != "" ] && DISTRO_VERSION=$(__parse_version_string "$rv")
fi

echoinfo "System Information:"
echoinfo "  CPU:          ${CPU_VENDOR_ID}"
echoinfo "  CPU Arch:     ${CPU_ARCH}"
echoinfo "  OS Name:      ${OS_NAME}"
echoinfo "  OS Version:   ${OS_VERSION}"
echoinfo "  Distribution: ${BS_DISTRO} ${DISTRO_VERSION}"

# Simplify distro name naming on functions
DISTRO_NAME_L=$(echo "$BS_DISTRO" | tr '[:upper:]' '[:lower:]' | sed 's/[^a-zA-Z0-9_ ]//g' | sed -Ee 's/([[:space:]])+/_/g')

# Simplify version naming on functions
if [ "$DISTRO_VERSION" = "" ]; then
    DISTRO_MAJOR_VERSION=""
else
    DISTRO_MAJOR_VERSION=$(echo "$DISTRO_VERSION" | sed 's/^\([0-9]*\).*/\1/g')
fi

# Fail soon for end of life versions
if [ "$DISTRO_MAJOR_VERSION" -lt 7 ]; then
    echoerror "End of life distributions are not supported. Please upgrade to the next stable." && exit 1
fi

echodebug "Binaries will be searched using the following \$PATH: ${PATH}"

# Let users know what's going to be installed
echoinfo "Installing minion"

# For debian versions, obtain the codename from the release version
case $DISTRO_MAJOR_VERSION in
    "7")  DISTRO_CODENAME="wheezy";;
    "8")  DISTRO_CODENAME="jessie";;
    "9")  DISTRO_CODENAME="stretch";;
    "10") DISTRO_CODENAME="buster";;
    *)    DISTRO_CODENAME="buster";;
esac

if [ "$(echo "${DISTRO_NAME_L}" | grep -E '(debian)')" = "" ] && [ "$ITYPE" = "stable" ] && [ "$STABLE_REV" != "latest" ]; then
    echoerror "${BS_DISTRO} does not have major version pegged packages support" && exit 1
fi

#----------------------------------------------------------------------------------------------------------------------
#  Apt Related Functions
#----------------------------------------------------------------------------------------------------------------------
__wait_for_apt(){
    # Timeout set at 15 minutes
    WAIT_TIMEOUT=900

    # Run our passed in apt command
    "${@}" 2>"$APT_ERR"
    APT_RETURN=$?

    # Make sure we're not waiting on a lock
    while [ $APT_RETURN -ne 0 ] && grep -q '^E: Could not get lock' "$APT_ERR"; do
        echoinfo "Aware of the lock. Patiently waiting $WAIT_TIMEOUT more seconds..."
        sleep 1
        WAIT_TIMEOUT=$((WAIT_TIMEOUT - 1))

        if [ "$WAIT_TIMEOUT" -eq 0 ]; then
            echoerror "Apt, apt-get, aptitude, or dpkg process is taking too long."
            echoerror "Bootstrap script cannot proceed. Aborting."
            return 1
        else
            "${@}" 2>"$APT_ERR"
            APT_RETURN=$?
        fi
    done

    return $APT_RETURN
}

__apt_get_install_noinput() {
    __wait_for_apt apt-get install -qq -o DPkg::Options::=--force-confold "${@}" > /dev/null; return $?
}

__apt_get_upgrade_noinput() {
    __wait_for_apt apt-get upgrade -qq -o DPkg::Options::=--force-confold > /dev/null; return $?
}

__temp_gpg_pub() {
    if __check_command_exists mktemp; then
        tempfile="$(mktemp /tmp/salt-gpg-XXXXXXXX.pub 2>/dev/null)"
        if [ -z "$tempfile" ]; then
            echoerror "Failed to create temporary file in /tmp"
            return 1
        fi
    else
        tempfile="/tmp/salt-gpg-$$.pub"
    fi

    echo $tempfile
}

__apt_key_fetch() {
    url=$1
    tempfile="$(__temp_gpg_pub)"

    __fetch_url "$tempfile" "$url" || return 1
    apt-key add "$tempfile" || return 1
    rm -f "$tempfile"

    return 0
}

#----------------------------------------------------------------------------------------------------------------------
#  Move File Function
#----------------------------------------------------------------------------------------------------------------------
__movefile() {
    if [ $# -eq 2 ]; then
        sfile=$1
        dfile=$2
    else
        echoerror "Wrong number of arguments for __movefile()"
        echoinfo "USAGE: __movefile <source> <dest>  OR  __movefile <source> <dest> <overwrite>"
        exit 1
    fi

    # Does the source file exist?
    if [ ! -f "$sfile" ]; then
        echowarn "$sfile does not exist!"
        return 1
    fi

    # If the destination is a directory, let's make it a full
    if [ -d "$dfile" ]; then
        echodebug "The passed destination($dfile) is a directory"
        dfile="${dfile}/$(basename "$sfile")"
        echodebug "Full destination path is now: $dfile"
    fi

    # The destination file does not exist, move
    if [ ! -f "$dfile" ]; then
        echodebug "Moving $sfile to $dfile"
        mv "$sfile" "$dfile" || return 1
    fi

    return 0
}

#----------------------------------------------------------------------------------------------------------------------
#  Service Check Functions
#----------------------------------------------------------------------------------------------------------------------
__check_services_systemd() {
    servicename=$1
    echodebug "Checking if service ${servicename} is enabled"

    if [ "$(systemctl is-enabled "${servicename}")" = "enabled" ]; then
        echodebug "Service ${servicename} is enabled"
        return 0
    else
        echodebug "Service ${servicename} is NOT enabled"
        return 1
    fi
}

#######################################################################################################################
#
#   In order to install salt-minion from salt-cloud for Debian you need:
#
#   1. install_debian_deps
#   2. config_salt
#   3. install_debian_stable
#   4. install_debian_check_services
#   5. install_debian_restart_daemons
#   6. daemons_running
#
__install_saltstack_debian_repository() {
    DEBIAN_RELEASE="$DISTRO_MAJOR_VERSION"
    DEBIAN_CODENAME="$DISTRO_CODENAME"

    __PY_VERSION_REPO="apt"

    if [ -n "$_PY_EXE" ] && [ "$_PY_MAJOR_VERSION" -eq 3 ]; then
        __PY_VERSION_REPO="py3"
    fi

    # Only py3 version are available on Debian 10+
    if [ "$DISTRO_MAJOR_VERSION" -ge 10 ]; then
        __PY_VERSION_REPO="py3"
    fi

    SALTSTACK_DEBIAN_URL="${BS_HTTP_VAL}://${_REPO_URL}/${__PY_VERSION_REPO}/debian/${DEBIAN_RELEASE}/${__REPO_ARCH}/${STABLE_REV}"
    echo "$__REPO_ARCH_DEB $SALTSTACK_DEBIAN_URL $DEBIAN_CODENAME main" > "/etc/apt/sources.list.d/saltstack.list"

    __apt_key_fetch "$SALTSTACK_DEBIAN_URL/SALTSTACK-GPG-KEY.pub" || return 1
    __wait_for_apt apt-get -qq update || return 1
}

install_debian_deps() {
    export DEBIAN_FRONTEND=noninteractive

    __wait_for_apt apt-get -qq update || return 1

    if [ "${_UPGRADE_SYS}" -eq $BS_TRUE ]; then
        __apt_get_upgrade_noinput || return 1
    fi

    if [ -n "$_PY_EXE" ] && [ "$_PY_MAJOR_VERSION" -eq 3 ]; then
        PY_PKG_VER=3
    else
        PY_PKG_VER=""
    fi

    # Dependencies
    __PACKAGES="${__PACKAGES}  python${PY_PKG_VER}-yaml"

    # shellcheck disable=SC2086
    __apt_get_install_noinput ${__PACKAGES} || return 1
    __install_saltstack_debian_repository || return 1
    return 0
}

install_debian_stable() {
    # shellcheck disable=SC2086
    __apt_get_install_noinput salt-minion || return 1
    return 0
}

install_debian_restart_daemons() {
    if [ -f /bin/systemctl ]; then
        /bin/systemctl stop salt-minion > /dev/null 2>&1
        /bin/systemctl start salt-minion.service
    fi
}

install_debian_check_services() {
    if [ -f /bin/systemctl ]; then
        __check_services_systemd salt-minion || return 1
    fi
    return 0
}

#----------------------------------------------------------------------------------------------------------------------
#  Default minion configuration function.
#----------------------------------------------------------------------------------------------------------------------
config_salt() {
    # If the configuration directory is not passed, return
    [ "$_TEMP_CONFIG_DIR" = "null" ] && return

    # Let's create the necessary directories
    [ -d "$_SALT_ETC_DIR" ] || mkdir "$_SALT_ETC_DIR" || return 1
    [ -d "$_PKI_DIR" ] || (mkdir -p "$_PKI_DIR" && chmod 700 "$_PKI_DIR") || return 1

    # Copy the grains file if found
    if [ -f "$_TEMP_CONFIG_DIR/grains" ]; then
        echodebug "Moving provided grains file from $_TEMP_CONFIG_DIR/grains to $_SALT_ETC_DIR/grains"
        __movefile "$_TEMP_CONFIG_DIR/grains" "$_SALT_ETC_DIR/grains" || return 1
    fi

    # Create the PKI directory
    [ -d "$_PKI_DIR/minion" ] || (mkdir -p "$_PKI_DIR/minion" && chmod 700 "$_PKI_DIR/minion") || return 1

    # Copy the minions configuration if found
    if [ -f "$_TEMP_CONFIG_DIR/minion" ]; then
        __movefile "$_TEMP_CONFIG_DIR/minion" "$_SALT_ETC_DIR" || return 1
    fi

    # Copy the minion's keys if found
    if [ -f "$_TEMP_CONFIG_DIR/minion.pem" ]; then
        __movefile "$_TEMP_CONFIG_DIR/minion.pem" "$_PKI_DIR/minion/" || return 1
        chmod 400 "$_PKI_DIR/minion/minion.pem" || return 1
    fi
    if [ -f "$_TEMP_CONFIG_DIR/minion.pub" ]; then
        __movefile "$_TEMP_CONFIG_DIR/minion.pub" "$_PKI_DIR/minion/" || return 1
        chmod 664 "$_PKI_DIR/minion/minion.pub" || return 1
    fi

    # For multi-master-pki, copy the master_sign public key if found
    if [ -f "$_TEMP_CONFIG_DIR/master_sign.pub" ]; then
        __movefile "$_TEMP_CONFIG_DIR/master_sign.pub" "$_PKI_DIR/minion/" || return 1
        chmod 664 "$_PKI_DIR/minion/master_sign.pub" || return 1
    fi

    return 0
}

#----------------------------------------------------------------------------------------------------------------------
#  This function checks if all of the installed daemons are running or not.
#----------------------------------------------------------------------------------------------------------------------
daemons_running() {
    FAILED_DAEMONS=0
    if [ "$(ps wwwaux | grep -v grep | grep salt-minion)" = "" ]; then
        echoerror "salt-$fname was not found running"
        FAILED_DAEMONS=$((FAILED_DAEMONS + 1))
    fi
    return $FAILED_DAEMONS
}

#======================================================================================================================
# LET'S PROCEED WITH OUR INSTALLATION
#
# Install dependencies
DEPS_INSTALL_FUNC="install_${DISTRO_NAME_L}_deps"
echoinfo "Running ${DEPS_INSTALL_FUNC}()"
if ! ${DEPS_INSTALL_FUNC}; then
    echoerror "Failed to run ${DEPS_INSTALL_FUNC}()!!!" && exit 1
fi

# Configure Salt
CONFIG_SALT_FUNC="config_salt"
echoinfo "Running ${CONFIG_SALT_FUNC}()"
if ! ${CONFIG_SALT_FUNC}; then
    echoerror "Failed to run ${CONFIG_SALT_FUNC}()!!!" && exit 1
fi

# Drop the master address if passed
if [ "$_SALT_MASTER_ADDRESS" != "null" ]; then
    [ ! -d "$_SALT_ETC_DIR/minion.d" ] && mkdir -p "$_SALT_ETC_DIR/minion.d"
    cat <<_eof > "$_SALT_ETC_DIR/minion.d/99-master-address.conf"
master: $_SALT_MASTER_ADDRESS
_eof
fi

# Drop the minion id if passed
if [ "$_SALT_MINION_ID" != "null" ]; then
    [ ! -d "$_SALT_ETC_DIR" ] && mkdir -p "$_SALT_ETC_DIR"
    echo "$_SALT_MINION_ID" > "$_SALT_ETC_DIR/minion_id"
fi

# Install Salt
INSTALL_FUNC="install_${DISTRO_NAME_L}_${ITYPE}"
echoinfo "Running ${INSTALL_FUNC}()"
if ! ${INSTALL_FUNC}; then
    echoerror "Failed to run ${INSTALL_FUNC}()!!!" && exit 1
fi

# Run any check services function, Only execute function if not in config mode only
CHECK_SERVICES_FUNC="install_${DISTRO_NAME_L}_check_services"
echoinfo "Running ${CHECK_SERVICES_FUNC}()"
if ! ${CHECK_SERVICES_FUNC}; then
    echoerror "Failed to run ${CHECK_SERVICES_FUNC}()!!!" && exit 1
fi

# Run any start daemons function
STARTDAEMONS_INSTALL_FUNC="install_${DISTRO_NAME_L}_restart_daemons"
echoinfo "Running ${STARTDAEMONS_INSTALL_FUNC}()"
echodebug "Waiting ${_SLEEP} seconds for processes to settle before checking for them"
sleep ${_SLEEP}
if ! ${STARTDAEMONS_INSTALL_FUNC}; then
    echoerror "Failed to run ${STARTDAEMONS_INSTALL_FUNC}()!!!" && exit 1
fi

# Check if the installed daemons are running or not
DAEMONS_RUNNING_FUNC="daemons_running"
echoinfo "Running ${DAEMONS_RUNNING_FUNC}()"
echodebug "Waiting ${_SLEEP} seconds for processes to settle before checking for them"
sleep ${_SLEEP}
if ! ${DAEMONS_RUNNING_FUNC}; then
    echoerror "Failed to run ${DAEMONS_RUNNING_FUNC}()!!!"

    if [ "$_ECHO_DEBUG" -eq $BS_FALSE ]; then
        echoerror "salt-minion was not found running. Pass '-D' to ${__ScriptName} when bootstrapping for additional debugging information..."
    fi

    [ ! -f "$_SALT_ETC_DIR/minion" ] && echodebug "$_SALT_ETC_DIR/minion does not exist"

    echodebug "Running salt-minion by hand outputs: $(nohup salt-minion -l debug)"

    [ ! -f /var/log/salt/minion ] && echodebug "/var/log/salt/minion does not exist. Can't cat its contents!"

    echodebug "DAEMON LOGS for minion:"
    echodebug "$(cat /var/log/salt/minion)"
    echo
    echodebug "Running Processes:"
    echodebug "$(ps auxwww)"

    exit 1
fi

echoinfo "Salt installed!"

exit 0

# vim: set sts=4 ts=4 et
