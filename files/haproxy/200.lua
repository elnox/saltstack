core.register_init(function ()
    core.log(core.info, "script loaded: http-response-ok")
end)

core.register_service("ok", "http", function (applet)
    local response = "OK"
    applet:set_status(200)
    applet:start_response()
    applet:send(response)
end)
