# Saltstack

## Abstract
The main objective of this repository is to help bootstrapping a small home lab using saltstack.

## Infrastructure

```
                                       Internet
                                           |
+-------------+                     +-------------+                              x
|    kali     | +-----------------> |   gateway   |                              |
+-------------+                     +------+------+                              |
                                           |                                     | net a: 192.168.0/16
                     +---------------------+---------------------+               |
                     |                     |                     |               |
              +------+------+       +------+------+       +------+------+        x
              |   blackbox  |       |   greenbox  |       |  whitebox   |
              |             |       |             |       |             |
              |  <lab vms>  |       |  <lab vms>  |       |  <lab vms>  |
              |             |       |             |       |             |
              +------+------+       +------+------+       +------+------+        x
                     |                     |                     |               | net b: 172.16.0/16
                     +---------------------+---------------------+               x
```

## Networking

```
gateway:
    eth0: 192.168.0.1/16

kali:
    eth0: 192.168.0.4/16

blackbox:
    br0: 192.168.1.1/16
    br1: 172.16.1.1/16

greenbox:
    br0: 192.168.3.1/16
    br1: 172.16.3.1/16

whitebox:
    br0: 192.168.2.1/16
    br1: 172.16.2.1/16
```

## Services

```
kali:
    bind9       [master]
    salt        [master]
    libvirtd
    docker

blackbox:
    bind9       [slave]
    gns3
    libvirtd
    docker
    keepalived  [vip: 192.168.0.100]
    haproxy     [bind: 192.168.0.100]

greenbox:
    bind9       [slave]
    gns3
    libvirtd
    docker
    registry    [private]
    keepalived  [vip: 192.168.0.100]
    haproxy     [bind: 192.168.0.100]

whitebox:
    bind9       [slave]
    gns3
    libvirtd
    docker
    keepalived  [vip: 192.168.0.100]
    haproxy     [bind: 192.168.0.100]
```
